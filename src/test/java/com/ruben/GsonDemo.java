package com.ruben;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: GsonDemo
 * @Description: 我还没有写描述
 * @Date: 2021/2/24 0024 20:55
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class GsonDemo {
    public static void main(String[] args) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        User user = new User();
        user.setFirstName("Supa");
        user.setLastName("Hino");
        user.setEmailAddress("achao1441470436@gmail.com");
        user.setPassword("39a8456c26584ba987d4a9f1f8f75fc1");
        String userJson = gson.toJson(user);
        System.out.println(userJson);
        String myJson = "{\"firstName\":\"Supa\",\"lastName\":\"Hino\",\"emailAddress\":\"achao1441470436@gmail.com\",\"password\":\"39a8456c26584ba987d4a9f1f8f75fc1\"}";
        User myUser = gson.fromJson(myJson, User.class);
        System.out.println(myUser);
    }

    @Data
    public static class User implements Serializable {
        private static final long serialVersionUID = 509877226276918727L;
        @Expose
        private String firstName;
        @Expose(serialize = false)
        private String lastName;
        @Expose(serialize = false, deserialize = false)
        private String emailAddress;
        private String password;
    }

}
