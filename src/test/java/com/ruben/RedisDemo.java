package com.ruben;

import com.alibaba.fastjson.TypeReference;
import com.ruben.manager.RedisManager;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.test.annotation.Rollback;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 我还没有写描述
 *
 * @author <achao1441470436@gmail.com>
 * @date 2021/4/29 0029 22:37
 */
@SpringBootTest
@Rollback(false)
@DependsOn("SpringContextHolder")
public class RedisDemo {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private RedisManager redisManager;


    @Test
    public void redisIncrementDemo() {
        RedisAtomicLong redisAtomicLong = Optional.ofNullable(stringRedisTemplate.getConnectionFactory())
                .map(factory -> new RedisAtomicLong("ruben", factory))
                .orElseThrow(() -> new RuntimeException("redis获取连接失败"));
        // 自增并获取 类比 ++i
        long longValue = redisAtomicLong.incrementAndGet();
        System.out.println("自增并获取" + longValue);        // 1
        // 获取并自增 类比 i++
        longValue = redisAtomicLong.getAndIncrement();
        System.out.println("获取并自增" + longValue);        // 1
        // 相加并获取 类比 +=
        longValue = redisAtomicLong.addAndGet(2L);
        System.out.println("相加并获取" + longValue);        // 4
        // 修改并获取
        longValue = redisAtomicLong.updateAndGet(i -> i + 2);
        System.out.println("修改并获取" + longValue);        // 6
        // 自减并获取 类比 --i
        longValue = redisAtomicLong.decrementAndGet();
        System.out.println("自减并获取" + longValue);        // 5
        // 计算和5的最大值并获取
        longValue = redisAtomicLong.accumulateAndGet(6L, Long::max);
        System.out.println("计算和5的最大值并获取" + longValue);  // 6
        // 计算两数相乘并获取
        longValue = redisAtomicLong.accumulateAndGet(5L, (left, right) -> new BigDecimal(left).multiply(new BigDecimal(right))
                .longValue());
        System.out.println("计算两数相乘并获取" + longValue);    //  30
        // 直接设置值（这里我们设为0表示重置）
        redisAtomicLong.set(0L);
        // 直接获取
        longValue = redisAtomicLong.get();
        System.out.println("获取" + longValue);           // 0
        // 设置过期时间30秒 过期后清除
        redisAtomicLong.expire(30, TimeUnit.SECONDS);
        redisAtomicLong.expire(Duration.of(30, ChronoUnit.SECONDS));
        // 获取过期时间
        Long expire = redisAtomicLong.getExpire();
        System.out.println("获取过期时间" + expire);      // 30
    }

    @Test
    public void managerTest() {
        List<String> userIds = redisManager.getFromRedisOrPutIntoMysql("userIds", () -> {
            // 从数据库中查询...
            return Arrays.asList("1", "2", "3");
        }, new TypeReference<List<String>>() {
        });
        System.out.println(userIds);
    }


}
