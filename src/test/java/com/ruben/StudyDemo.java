package com.ruben;/**
 * @ClassName: StudyDemo
 * @Date: 2020/11/18 0018 21:51
 * @Description:
 */

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.AES;
import com.ruben.pojo.po.UserPO;
import com.ruben.pojo.vo.UserVO;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @ClassName: StudyDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/18 0018 21:51
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class StudyDemo {


    private static final List<String> list = new ArrayList<>(Integer.MAX_VALUE / 2);
    private int stackLength = 1;

    private static void encrypt() {
        // 生成 16 位随机 AES 密钥
        String randomKey = AES.generateRandomKey();
        // 随机密钥加密
        String url = AES.encrypt("jdbc:mysql://localhost:3306/ruben?autoReconnect=true&zeroDateTimeBehavior=CONVERT_TO_NULL&useUnicode=true&characterEncoding=utf-8&useSSL=false&nullCatalogMeansCurrent=true&serverTimezone=Asia/Shanghai&allowMultiQueries=true", randomKey);
        String username = AES.encrypt("root", randomKey);
        String password = AES.encrypt("789456", randomKey);
        System.out.println("randomKey：[ " + randomKey + " ]");
        System.out.println("url：[ mpw:" + url + " ]");
        System.out.println("username：[ mpw:" + username + " ]");
        System.out.println("password：[ mpw:" + password + " ]");
    }

    private static void stackOverflow() {
        StudyDemo oom = new StudyDemo();
        try {
            oom.addStackLength();
        } catch (Throwable e) {
            System.out.println("stack length:" + oom.stackLength);
            throw e;
        }
    }

    private static void hashMapPrint() throws Exception {
        //指定初始容量15来创建一个HashMap
        HashMap m = new HashMap(1);
        //获取HashMap整个类
        Class<?> mapType = m.getClass();
        //获取指定属性，也可以调用getDeclaredFields()方法获取属性数组
        Field threshold = mapType.getDeclaredField("threshold");
        //将目标属性设置为可以访问
        threshold.setAccessible(true);
        //获取指定方法，因为HashMap没有容量这个属性，但是capacity方法会返回容量值
        Method capacity = mapType.getDeclaredMethod("capacity");
        //设置目标方法为可访问
        capacity.setAccessible(true);
        //打印刚初始化的HashMap的容量、阈值和元素数量
        System.out.println("容量：" + capacity.invoke(m) + "    阈值：" + threshold.get(m) + "    元素数量：" + m.size());
        for (int i = 0; i < 17; i++) {
            m.put(i, i);
            //动态监测HashMap的容量、阈值和元素数量
            System.out.println("容量：" + capacity.invoke(m) + "    阈值：" + threshold.get(m) + "    元素数量：" + m.size());
        }
    }

    public void addStackLength() {
        stackLength++;
//        addStackLength();
    }

    public void addHeapString() {
        list.add(" 1");
//        addHeapString();
    }


    private static void isSynthetic() throws NoSuchFieldException {
        System.out.println(Ruben.class.getDeclaredField("$VALUES").isSynthetic());
        System.out.println(((Consumer) (c) -> System.gc()).getClass().isSynthetic());
        System.out.println(((Supplier) System::nanoTime).getClass().isSynthetic());
        System.out.println(((Predicate) (p) -> Boolean.FALSE).getClass().isSynthetic());
        System.out.println(((Function) (f) -> "ruben").getClass().isSynthetic());
        System.out.println(((Runnable) new Runnable() {
            @Override
            public void run() {
            }
        }).getClass().isSynthetic());
        System.out.println(((Runnable) System::nanoTime).getClass().isSynthetic());
    }

    private static void intern() {
        String a = "abcdefg";
        char[] chars = a.toCharArray();
        char[] result = new char[chars.length];
        System.arraycopy(chars, 0, result, 0, chars.length);
        for (char c : result) {
            System.out.print(c);
        }
        System.gc();
        String intern = a.intern();
        System.out.println("\n" + intern);
        String Str1 = "RUBEN";
        String Str2 = "ruben";
        System.out.println(Str1.intern());
        System.out.println(Str2.intern());
    }


    private enum Ruben {
    }

}
