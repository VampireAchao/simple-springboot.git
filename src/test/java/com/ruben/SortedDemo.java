package com.ruben;/**
 * @ClassName: SortedDemo
 * @Date: 2020/11/1 0001 17:33
 * @Description:
 */

import org.assertj.core.util.Lists;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @ClassName: SortedDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/1 0001 17:33
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class SortedDemo {
    public static final AtomicBoolean ASC = new AtomicBoolean(true);

    public static void main(String[] args) {
        List<List<Integer>> listList = Lists.newArrayList();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            listList.add(Arrays.asList(random.nextInt(20), random.nextInt(30), random.nextInt(40)));
        }
        AtomicInteger up = new AtomicInteger(1);
        AtomicInteger down = new AtomicInteger(-1);
        AtomicReference<Comparator<Integer>> orderRule = new AtomicReference<>(Comparator.naturalOrder());
        if (!ASC.get()) {
            up.set(-1);
            down.set(1);
            orderRule.set(Comparator.reverseOrder());
        }
        listList = listList.stream().peek(list -> list.sort(orderRule.get())).sorted((o1, o2) -> {
            if (o1.isEmpty()) return down.get();
            if (o2.isEmpty()) return up.get();
            for (int i = 0; i < Math.min(o1.size(), o2.size()); i++) {
                Integer integer1 = Optional.ofNullable(o1.get(i)).orElse(0);
                Integer integer2 = Optional.ofNullable(o2.get(i)).orElse(0);
                if (integer1 > integer2) return up.get();
                if (integer1 < integer2) return down.get();
            }
            return Integer.compare(o1.size(), o2.size());
        }).collect(Collectors.toList());
        listList.forEach(list -> {
            list.forEach(i -> System.out.print(i + " "));
            System.out.println();
        });
    }
}
