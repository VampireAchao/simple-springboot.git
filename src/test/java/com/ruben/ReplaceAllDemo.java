package com.ruben;

/**
 * @ClassName: ReplaceAllDemo
 * @Description: 我还没有写描述
 * @Date: 2021/2/19 0019 20:56
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class ReplaceAllDemo {
    public static void main(String[] args) {
        String str = "Hino Supa and ruben";
        System.out.println(str.replace("[idea]", ""));
        System.out.println(str.replaceAll("[idea]", ""));
    }
}
