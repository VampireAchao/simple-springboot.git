package com.ruben;

import com.ruben.config.RabbitmqConfig;
import com.ruben.pojo.dto.EmailDataTransferObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;

import javax.annotation.Resource;

/**
 * @ClassName: RabbitMQDemo
 * @Description: 我还没有写描述
 * @Date: 2021/2/19 0019 22:10
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Slf4j
@SpringBootTest
@DependsOn("SpringContextHolder")
public class RabbitMQDemo {

    @Resource
    private AmqpTemplate amqpTemplate;

    @Test
    public void producer() {
        EmailDataTransferObject emailDataTransferObject = EmailDataTransferObject.builder().code("123").build();
        amqpTemplate.convertAndSend(RabbitmqConfig.EXCHANGE_RUBEN, RabbitmqConfig.ROUTING_KEY_RUBEN, emailDataTransferObject);
    }

    @Test
    public void consumer() {
        EmailDataTransferObject emailDataTransferObject = (EmailDataTransferObject) amqpTemplate.receiveAndConvert(RabbitmqConfig.QUEUE_RUBEN_SMS);
        log.info(String.valueOf(emailDataTransferObject));
    }
}
