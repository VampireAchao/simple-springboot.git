package com.ruben;/**
 * @ClassName: BuilderDemo
 * @Date: 2020/11/8 0008 13:09
 * @Description:
 */

import lombok.Builder;

/**
 * @ClassName: BuilderDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/8 0008 13:09
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class BuilderDemo {

    public static void main(String[] args) {
        Teacher teacher = Teacher.builder().name("achao").age(91).build();
        System.out.println(teacher);
        Student ruben = Student.builder().name("ruben").age(19).build();
        System.out.println(ruben);
    }

    /**
     * 自定义pojo
     */
    @Builder
    public static class Teacher {
        private final String name;
        private final Integer age;

        @Override
        public String toString() {
            return "Teacher{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    /**
     * 自定义pojo
     */
    public static class Student {
        private final String name;
        private final Integer age;

        Student(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        private static StudentBuilder builder() {
            return new StudentBuilder();
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }

        public static class StudentBuilder {
            private String name;
            private Integer age;

            StudentBuilder() {
            }

            public StudentBuilder name(String name) {
                this.name = name;
                return this;
            }

            public StudentBuilder age(Integer age) {
                this.age = age;
                return this;
            }

            public Student build() {
                return new Student(name, age);
            }

            @Override
            public String toString() {
                return "StudentBuilder{" +
                        "name='" + name + '\'' +
                        ", age=" + age +
                        '}';
            }
        }

    }

}
