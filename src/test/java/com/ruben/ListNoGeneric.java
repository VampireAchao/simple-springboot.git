package com.ruben;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName: ListNoGeneric
 * @Date: 2020/9/16 21:04
 * @Description:
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class ListNoGeneric {
    public static void main(String[] args) {
        // 生成从0到20的数字，过滤掉奇数
        List<Integer> intList = Stream.iterate(0, e -> ++e).limit(20).collect(Collectors.toList());
        long filterStart = System.nanoTime();
        intList.stream().filter(a -> a % 2 == 0).collect(Collectors.toList());
        long filterEnd = System.nanoTime();
        System.out.println("filter执行了" + ((filterEnd - filterStart) / (1000.0 * 1000.0)) + "ms");
        long removeIfStart = System.nanoTime();
        intList.removeIf(a -> a % 2 != 0);
        long removeIfEnd = System.nanoTime();
        System.out.println("removeIfEnd执行了" + ((removeIfEnd - removeIfStart) / (1000.0 * 1000.0)) + "ms");

    }

    private static void jsonArray() {
        JSONObject jsonObject = JSONObject.parseObject("{\"level\":[\"3\"]}");
        List<Integer> intList = new ArrayList<Integer>(10);

        if (jsonObject != null) {
            intList.addAll(JSON.parseArray(jsonObject.getString("level"), Integer.class));
            int amount = 0;
            for (Integer integer : intList) {
                System.out.println(integer);
                if (true) {
                    amount = amount + 1;
                }
            }
        }
    }

    private static void listNoGeneric() {
        List a1 = new ArrayList<>();
        a1.add(new Object());
        a1.add(new Integer(111));
        a1.add("hello a1a1");

        a1.forEach(System.out::println);

        List<Object> a2 = a1;
        a2.add(new Object());
        a2.add(new Integer(222));
        a2.add("hello a2a2");
        a2.forEach(System.out::println);

        List<Integer> a3 = a1;
        a3.add(new Integer(333));
//        a3.add(new Object());
//        a3.add(new String("hello a3a3"));
//        a3.forEach(System.out::println);

        List<?> a4 = a1;
        a1.remove(0);
        a1.clear();
//        a4.add(new Object());
    }
}
