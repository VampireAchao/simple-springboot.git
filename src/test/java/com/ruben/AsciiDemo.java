package com.ruben;

/**
 * Ascii例子
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021/6/17 0017 21:47
 */
public class AsciiDemo {
    public static void main(String[] args) {
        /*System.out.println(new Character('A').hashCode());
        System.out.println(Character.hashCode('A'));
        System.out.println((int) 'A');
        System.out.println((int) new Character('A'));*/


/*        System.out.println((char) 65);
        System.out.println(new Character((char) 65));
        System.out.println((Character) (char) 65);
        System.out.println((Character) (char) (int) new Integer(65));*/

        System.out.println('A' == 65);

        /*System.out.println((Integer) (int) (char) (Object) (Character) (char) 65);
        System.out.println((Character) (char) (int) (Object) (Integer) (int) 'A');
        int word = 'A';
        Integer word2 = 66;
        Character word3 = 66;
        System.out.println(word = word2 = (Integer) (int) word3);
        System.out.println((char) (word = ~word2));*/
    }
}
