package com.ruben;

import com.sun.xml.internal.fastinfoset.Encoder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * 我还没有写描述
 *
 * @author <achao1441470436@gmail.com>
 * @date 2021/4/24 0024 15:11
 */
public class EncodeDemo {
    public static void main(String[] args) throws UnsupportedEncodingException {
        // URL编码
        String encode = URLEncoder.encode("你好，世界", Encoder.UTF_8);
        System.out.println(encode);
        // URL解码
        String decode = URLDecoder.decode(encode, Encoder.UTF_8);
        System.out.println(decode);
    }
}
