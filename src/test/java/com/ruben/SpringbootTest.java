package com.ruben;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruben.dao.MpUserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @ClassName: SpringbootTest
 * @Description: 我还没有写描述
 * @Date: 2020/11/29 0029 17:10
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@SpringBootTest(classes = {SimpleSpringbootApplication.class})
public class SpringbootTest {

    @Resource
    private MpUserMapper mpUserMapper;

    @Test
    public void test() {
        mpUserMapper.selectList(Wrappers.emptyWrapper());
    }

}
