package com.ruben;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruben.utils.FileUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * @ClassName: PrisonDemo
 * @Date: 2020/9/26 10:35
 * @Description:
 */
public class PrisonDemo {
    public static void main(String[] args) throws IOException {
        parseImg(1, 10);
    }

    public static List parseImg(int pageNum, int pageSize) throws IOException {
        String url = "http://yywallpaper.top/query/picture";
        Connection connection = Jsoup
                .connect(url)
                .ignoreContentType(true)
                .timeout(60000);
        connection.data("picType", "3");
        connection.data("pageNum", String.valueOf(pageNum));
        connection.data("pageSize", String.valueOf(pageSize));
        Document document = connection.post();
        Elements body = document.getElementsByTag("body");
        String jsonString = body.text();
        JSONArray elements = JSON.parseObject(jsonString).getJSONArray("elements");
        for (int i = 0; i < pageSize; i++) {
            JSONObject jsonObject = JSON.parseObject(elements.get(i).toString());
            String image = jsonObject.getString("bigUrl");
            String fileName = image.substring(image.lastIndexOf("/"));
            System.out.println(fileName.substring(1));
            FileUtils.downloadPicture(image, "C:\\Users\\Doyle\\Desktop\\file\\image" + fileName);
        }
        return null;
    }

}
