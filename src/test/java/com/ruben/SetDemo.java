package com.ruben;

import java.util.Arrays;
import java.util.HashSet;

/**
 * @ClassName: SetDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/22 0022 21:28
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class SetDemo {
    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<>(Arrays.asList("0", "1", "2"));
        HashSet<String> hashSet2 = new HashSet<>(Arrays.asList("1", "2", "3"));
        // 取交集
        hashSet.retainAll(hashSet2);
        hashSet.forEach(System.out::println);
        System.out.println();
        HashSet<String> hashSet3 = new HashSet<>(Arrays.asList("0", "1", "2"));
        // 取并集
        hashSet3.addAll(hashSet2);
        hashSet3.forEach(System.out::println);
        System.out.println();
        HashSet<String> hashSet4 = new HashSet<>(Arrays.asList("0", "1", "2"));
        // 取差集
        hashSet4.removeAll(hashSet2);
        hashSet4.forEach(System.out::println);
    }
}
