package com.ruben;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruben.dao.MpUserMapper;
import com.ruben.pojo.User;
import com.ruben.utils.TextValidator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.annotation.Rollback;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.lang.reflect.Proxy;
import java.util.Objects;
import java.util.Set;

@SpringBootTest
@Rollback(false)
@DependsOn("SpringContextHolder")
class SimpleSpringbootApplicationTests {

    @Resource
    private Validator validator;

    @Resource
    private HttpServletRequest httpServletRequest;

    @Test
    void sessionTest() {
        httpServletRequest.getSession().setAttribute("testSession", "I need this");
    }

    @Test
    void test() {
        User user = User.builder().build();
        // 校验结果
        Set<ConstraintViolation<User>> checkResult;
        if (Objects.isNull(user.getId())) {
            // id为空，新增校验
            checkResult = validator.validate(user, User.AddCheck.class);
        } else {
            // id不为空，修改校验
            checkResult = validator.validate(user, User.UpdateCheck.class);
        }
        if (!checkResult.isEmpty()) {
            // 这里可以抛异常，让全局异常处理器去处理我们的异常
            throw new ConstraintViolationException(checkResult);
        }
    }

    @Resource(name = "myFactoryBean")
    private User ruben;

    @Test
    void proxy() {
        MpUserMapper mapper = (MpUserMapper) Proxy.newProxyInstance(MpUserMapper.class.getClassLoader(),
                new Class[]{MpUserMapper.class},
                (proxy, method, args) -> {
                    System.out.println("执行查询");
                    return null;
                });
        mapper.selectOne(Wrappers.lambdaQuery());
        System.out.println(ruben.getUsername());
    }


    @Test
    public void textValidator() {
    }


}
