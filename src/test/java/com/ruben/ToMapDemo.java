package com.ruben;

import com.ruben.pojo.User;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @ClassName: ToMapDemo
 * @Description: 我还没有写描述
 * @Date: 2021/2/4 0004 20:07
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class ToMapDemo {
    public static void main(String[] args) throws ClassNotFoundException {
        List<User> userList = new ArrayList<>(10);
        Map<Integer, User> userMap = userList.stream().collect(Collectors.toMap(User::getId, Function.identity(), (user1, user2) -> user2));
        userMap = userList.stream().collect(Collectors.toMap(User::getId, Function.identity(), (user1, user2) -> user2, LinkedHashMap::new));
        Class<User> userClass = User.class;
        Class<? extends User> aClass = User.builder().build().getClass();
        Class<?> aClass1 = Class.forName("com.ruben.pojo.User");
        Class<?> aClass2 = Class.forName("com.ruben.pojo.User", true, userClass.getClassLoader());
    }
}
