package com.ruben;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @ClassName: AsyncDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/30 0030 21:17
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@SpringBootTest
public class AsyncDemo {

    @Test
    public void test() throws ExecutionException, InterruptedException {
        Future<StreamDemo.User> future = whoAmI("ruben", 19);
        StreamDemo.User user = future.get();
        System.out.println(user);
    }

    @Async
    public Future<StreamDemo.User> whoAmI(String ruben, Integer age) {
        return new AsyncResult<>(StreamDemo.User.builder().name(ruben).age(age).build());
    }

}
