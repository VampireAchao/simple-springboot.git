package com.ruben;

import com.ruben.task.GoodJob;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.*;

/**
 * @ClassName: ScheduleTest
 * @Date: 2021/1/20 0020 16:15
 * @Description: Schedule
 * @Author: <achao1441470436@gmail.com>
 */
@Slf4j
@SpringBootTest
public class ScheduleTest {

    @Resource
    private Scheduler scheduler;

    @Test
    public void run() throws Exception {
        // scheduler可以通过StdSchedulerFactory去获取，也可以使用spring的注解@Resource/Autowired引用
//        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        // Java虚拟机运行时间，以纳秒为单位
        long start = System.nanoTime();
        // JobDetail的key(唯一标识)，由name和group组成
        JobKey jobKey = new JobKey("achao", "ruben");
        // 删除jobKey对应的JobDetail
        scheduler.deleteJob(jobKey);
        // 构建JobDetail，包含任务类、唯一标识、需要发送的数据等信息
        JobDetail jobDetail = JobBuilder.newJob()
                // 描述
                .withDescription("ruben->achao->jobDetail")
                // 指向我们的任务类,需要实现Job接口
                .ofType(GoodJob.class)
                // 唯一标识
                .withIdentity(jobKey)
                // 传入发给Job的数据
                .usingJobData("name", "achao")
                .usingJobData("time", start)
                .build();
        // 构建Trigger，包含执行间隔时间、次数等。许多Trigger可以指向一个Job ，但是一个Trigger只能指向一个Job。
        // 每5秒后执行一次，执行次数无限
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("achao", "ruben").withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5).repeatForever()).startNow().build();
        // 每五秒一次，执行次数10次
//        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("achao", "ruben").withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5).withRepeatCount(10)).startNow().build();
        // 指定时间点触发
        Date executeDate = new Date(System.currentTimeMillis() + 5000);
        TriggerBuilder.newTrigger().startAt(executeDate).withIdentity("achao", "ruben").build();
        // 使用cron构建，这里就可以做动态cron
//        CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity("achao", "ruben").withSchedule(CronScheduleBuilder.cronSchedule("*/5 * * * * ?")).startNow().build();
        // 将给定的JobDetail添加到Scheduler，并将给定的Trigger与其关联。如果给定的触发器未引用任何Job ，则将其设置为引用与此方法一起传递的Job。
        scheduler.scheduleJob(jobDetail, trigger);
        // 开始执行
        scheduler.start();
        log.info("开始" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
        // 10秒后执行
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    // 暂停
                    scheduler.pauseJob(jobKey);
                    log.info("已暂停" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        }, 10000);
        // 20秒后执行
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    // 继续暂停的job
                    scheduler.resumeJob(jobKey);
                    log.info("已继续" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
                    // 立马执行一次job
                    scheduler.triggerJob(jobKey);
                    log.info("立马执行" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        }, 20000);
        // 30秒后执行
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    // 删除任务
                    scheduler.deleteJob(jobKey);
                    log.info("结束[" + jobKey.getGroup() + "." + jobKey.getName() + "] ：" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
                    // 判断Scheduler是否处于待机模式
                    if (!scheduler.isInStandbyMode()) {
                        // 暂停调度器
                        scheduler.standby();
                        log.info("暂停调度器" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
//                        scheduler.start();
                    }
                    // 判断Scheduler是否已关闭
                    if (!scheduler.isShutdown()) {
                        // 结束调度器
                        scheduler.shutdown(true);
                        log.info("结束调度器" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
                    }
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        }, 30000);
        try {
            // 封装条件，查询所有
            GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
            // 查询job
            Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
            System.out.println("任务列表:");
            jobKeys.forEach(key -> System.out.println(key.getGroup() + "." + key.getName()));
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        // 获取当前正在执行的所有job列表
        List<JobExecutionContext> jobs = scheduler.getCurrentlyExecutingJobs();
        jobs.forEach(job -> System.out.println("class:" + job.getTrigger().getClass().getName() + " group:" + job.getTrigger().getJobKey().getGroup() + job.getTrigger().getJobKey().getName()));
        // 25秒后执行
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    // 重置任务（修改）
                    TriggerKey key = TriggerKey.triggerKey("achao", "ruben");
                    CronTrigger newTrigger = TriggerBuilder.newTrigger().withIdentity(key).withSchedule(CronScheduleBuilder.cronSchedule("*/1 * * * * ?")).build();
                    scheduler.rescheduleJob(key, newTrigger);
                    log.info("重置任务" + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        }, 25000);
        // 获取输入，挂起程序，避免还没有执行到我们的逻辑就结束，在实际web项目中去掉
        new Scanner(System.in).next();
    }

}
