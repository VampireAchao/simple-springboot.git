package com.ruben;/**
 * @ClassName: LangDemo
 * @Date: 2020/11/16 0016 22:03
 * @Description:
 */

import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName: LangDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/16 0016 22:03
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class LangDemo {
    public static void main(String[] args) {
        System.out.println(StringUtils.abbreviate("这也在你的预料之中吗JOJO", "...", 0, 12));
    }
}
