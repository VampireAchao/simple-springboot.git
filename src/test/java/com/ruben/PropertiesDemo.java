package com.ruben;

import com.ruben.pojo.RubenProperties;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @ClassName: PropertiesDemo
 * @Description: 我还没有写描述
 * @Date: 2021/2/16 0016 11:29
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Slf4j
@SpringBootTest
public class PropertiesDemo {

    @Resource
    private RubenProperties rubenProperties;

    @Test
    public void test() {
        log.info(rubenProperties.toString());
    }

}
