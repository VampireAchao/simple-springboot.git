package com.ruben;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName: IteratorDemo
 * @Description: 我还没有写描述
 * @Date: 2021/3/5 0005 22:16
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class IteratorDemo {
    public static void main(String[] args) {
        // 构造从0到20的list
        List<Integer> list = Stream.iterate(0, i -> ++i).limit(20).collect(Collectors.toList());
        // 删除
//        list.forEach(list::remove); // 导致java.util.ConcurrentModificationException
//        list.removeIf(i -> i.equals(i));
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            Integer nowNumber = iterator.next();
            if (nowNumber > 10) {
                iterator.remove();
            }
        }
    }
}
