package com.ruben;

import java.io.*;
import java.util.Optional;

/**
 * @ClassName: FileDemo
 * @Description: 我还没有写描述
 * @Date: 2021/1/10 0010 21:38
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class FileDemo {
    private static final String FILE_PATH = "D:/file/files/ps/2077.png";
    private static final String TARGET_PATH = "D:/file/files/ps/1977.png";

    public static void main(String[] args) {
        try (
                InputStream dataInputStream = new FileInputStream(FILE_PATH);
                OutputStream output = new FileOutputStream(TARGET_PATH)
        ) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = dataInputStream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



    /*public static void main(String[] args) {
        byte[] result;
        try (
                InputStream dataInputStream = new FileInputStream(FILE_PATH);
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                OutputStream dataOutputStream = new FileOutputStream(TARGET_PATH)
        ) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = dataInputStream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            result = output.toByteArray();
            // 写入文件
            dataOutputStream.write(result);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/
