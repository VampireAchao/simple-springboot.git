package com.ruben;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruben.enumeration.GenderEnum;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * 我还没有写描述
 *
 * @author <achao1441470436@gmail.com>
 * @date 2021/4/12 0012 23:17
 */
public class JacksonDemo {
    @SneakyThrows
    public static void main(String[] args) {
        Instant from = LocalDateTime.parse("2021-01-09 00:00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)).toInstant(ZoneOffset.MAX);
        Student supa = Student.builder().name("supa").age(20).gender(GenderEnum.MALE).birthday(Date.from(from)).build();
        System.out.println(new ObjectMapper().writeValueAsString(supa));
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE,
            isGetterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY,
            setterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY,
            creatorVisibility = JsonAutoDetect.Visibility.NON_PRIVATE,
            fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
    private static class Student implements Serializable {
        private static final long serialVersionUID = -3289647584974663707L;
        public String name;
        private Integer age;
        private String job;
        private GenderEnum gender;
        private Date birthday;
        private String json;
    }

}
