package com.ruben;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static cn.hutool.core.util.CharsetUtil.GBK;

/**
 * 我还没有写描述
 *
 * @author <achao1441470436@gmail.com>
 * @date 2021/5/4 0004 19:23
 */
public class CommandDemo {
    @SneakyThrows
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        Process process = Runtime.getRuntime().exec("cmd.exe /c DIR");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream(), GBK));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        System.out.println(stringBuilder.toString());
    }
}
