package com.ruben;

/**
 * @author <achao1441470436@gmail.com>
 * @since 2021/6/8 0008 22:00
 */
public class ClassDemo {
    public static void main(String[] args) {
        Class<Integer> integerClass = int.class;
        System.out.println("int.class是否为基本类型：" + integerClass.isPrimitive());

        Class<Integer> boxIntegerClass = Integer.class;
        System.out.println("Integer.class是否为基本类型：" + boxIntegerClass.isPrimitive());

        System.out.println("八大基本类型");
        System.out.println(int.class.isPrimitive());
        System.out.println(short.class.isPrimitive());
        System.out.println(long.class.isPrimitive());
        System.out.println(byte.class.isPrimitive());
        System.out.println(char.class.isPrimitive());
        System.out.println(boolean.class.isPrimitive());
        System.out.println(float.class.isPrimitive());
        System.out.println(double.class.isPrimitive());

    }
}
