package com.ruben;/**
 * @ClassName: GroupDemo
 * @Date: 2020/11/6 0006 22:16
 * @Description:
 */

import com.ruben.pojo.User;
import org.assertj.core.util.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName: GroupDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/6 0006 22:16
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class GroupDemo {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0));
        Map<Boolean, List<Integer>> collect = integerList.stream().collect(Collectors.groupingBy(data -> data % 2 == 0));
        List<Integer> evenNumbers = collect.get(true);
        List<Integer> oddNumber = collect.get(false);
        System.out.println("data % 2 == 0-----------");
        evenNumbers.forEach(System.out::println);
        System.out.println("data % 2 != 0-----------");
        oddNumber.forEach(System.out::println);
        List<User> userList = Lists.newArrayList();
        userList.add(new User("ruben", "1"));
        userList.add(new User("achao", "2"));
        userList.add(new User("ruben", "3"));
        Map<String, List<User>> userMap = userList.stream().collect(Collectors.groupingBy(User::getUsername));
        List<User> ruben = userMap.get("ruben");
        System.out.println("ruben-----------");
        ruben.forEach(System.out::println);
        List<User> achao = userMap.get("achao");
        System.out.println("achao-----------");
        achao.forEach(System.out::println);
    }
}
