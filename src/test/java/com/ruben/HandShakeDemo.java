package com.ruben;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @ClassName: HandShakeDemo
 * @Date: 2020/9/25 19:49
 * @Description:
 */
public class HandShakeDemo {


    /**
     * 标志位需要定义在函数外边
     */
    public static boolean antiShakeFlag = false;

    public static void main(String[] args) {
        int a = 9;
        int b = 4;
        System.out.println((a % b) == (a & (b - 1)));
        for (int i = 1; i <= 10; i++) {
            System.out.println("循环第" + i + "次");
            ruben();
        }
    }

    /**
     * 执行的函数
     */
    public static void ruben() {
        if (antiShakeFlag) {
            return;
        }
        try {
            // 执行逻辑
            System.out.println("ruben执行了");
            // 逻辑执行完毕后置为空
            antiShakeFlag = true;
            new Timer().schedule(new TimerTask() {
                public void run() {
                    antiShakeFlag = false;
                }
            }, 5000);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
