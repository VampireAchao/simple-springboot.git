package com.ruben;

/**
 * @ClassName: ProgressBarDemo
 * @Date: 2020/9/21 19:36
 * @Description:
 */
public class ProgressBarDemo {
    public static void main(String[] args) {
        for (int i = 0; i < 100 * 100 * 100; i++) {
            System.out.print(i);
            System.out.print("\r");
        }
    }

    private static void progressBar() {
        final long size = 1000L;
        for (int i = 0; i < 101; i++) {
            String tu = "▅";
            for (int j = 0; j < i / 10; j++) {
                tu += "▅";
            }
            System.out.print("\r当前进度：" + (i) + "%\t" + tu + "\t" + (i * 10) + "/" + size);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
