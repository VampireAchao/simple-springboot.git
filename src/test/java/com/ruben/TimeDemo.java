package com.ruben;

import com.ruben.utils.CachingDateFormatter;
import org.apache.commons.lang3.time.FastDateFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @ClassName: TimeDemo
 * @Description: 我还没有写描述
 * @Date: 2021/1/6 0006 19:56
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class TimeDemo {
    private static final String PATTERN = "yyyy-MM-dd HH:mm:SSS";
    private static final ThreadLocal<DateFormat> df = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd"));

    public static void main(String[] args) {
    }

    private static void lastTest() {
        System.out.println("下面的是两个默认时区的LocalDateTime");
        final LocalDateTime localDateTime = LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault());
        final LocalDateTime localDateTime1 = LocalDateTime.now();
        System.out.println(localDateTime);
        System.out.println(localDateTime1);
        System.out.println("下面的是两个通过Date.from()拿到的默认时区的Date");
        // 这里需要使用ZonedDateTime
        final Date from = Date.from(Instant.from(ZonedDateTime.now()));
        // 这里需要使用OffsetDateTime
        final Date from1 = Date.from(localDateTime1.toInstant(OffsetDateTime.now().getOffset()));
        System.out.println(from);
        System.out.println(from1);
        System.out.println("下面的是通过LocalDateTime.ofInstant()拿到的不同时区的LocalDateTime");
        // 国际标准时间
        final LocalDateTime localDateTime2 = LocalDateTime.ofInstant(Instant.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)), ZoneId.systemDefault());
        // 自定义偏移时区 东8区(北京时区) 支持 +h, +hh, +hhmm, +hh:mm, +hhmmss, +hh:mm:ss
        final LocalDateTime localDateTime3 = LocalDateTime.ofInstant(Instant.from(LocalDateTime.now().toInstant(ZoneOffset.of("+08:00"))), ZoneId.systemDefault());
        System.out.println(localDateTime2);
        System.out.println(localDateTime3);
        System.out.println("下面的是通过Date.from()拿到的不同时区的date");
        // 国际标准时间
        final Date from2 = Date.from(Instant.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        // 最小时区
        final Date from3 = Date.from(Instant.from(LocalDateTime.now().toInstant(ZoneOffset.MIN)));
        // 最大时区
        final Date from4 = Date.from(Instant.from(LocalDateTime.now().toInstant(ZoneOffset.MAX)));
        // 自定义偏移时区 东8区(北京时区) 支持 +h, +hh, +hhmm, +hh:mm, +hhmmss, +hh:mm:ss
        final Date from5 = Date.from(Instant.from(LocalDateTime.now().toInstant(ZoneOffset.of("+8"))));
        System.out.println(from2);
        System.out.println(from3);
        System.out.println(from4);
        System.out.println(from5);
    }

    private static void previous() {
        // 原始方式
        long dateTimeStart = System.nanoTime();
        Stream.generate(() -> LocalDateTime.now().format(DateTimeFormatter.ofPattern(PATTERN))).limit(10000).collect(Collectors.toList());
        long dateTimeEnd = System.nanoTime();
        System.out.println((dateTimeEnd - dateTimeStart) / (1000.0 * 1000.0) + "ms");

        // 工具类，手动获取实例
        long start = System.nanoTime();
        Stream.generate(() -> new CachingDateFormatter(FastDateFormat.getInstance(PATTERN)).format(System.currentTimeMillis())).limit(10000).collect(Collectors.toList());
        long end = System.nanoTime();
        System.out.println((end - start) / (1000.0 * 1000.0) + "ms");

        // 工具类，自动获取实例
        long patternStart = System.nanoTime();
        Stream.generate(() -> new CachingDateFormatter(PATTERN).format(System.currentTimeMillis())).limit(10000).collect(Collectors.toList());
        long patternEnd = System.nanoTime();
        System.out.println((patternEnd - patternStart) / (1000.0 * 1000.0) + "ms");
    }

    private static void old() {
        // Date转String
        Date date = new Date();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        // Date转英文年月日星期时间 Wed 06 January 2021 20:30 PM
        System.out.println(localDateTime.format(DateTimeFormatter.ofPattern("E dd MMMM yyyy hh:mm a", Locale.US)));
        // Date转中文年月日星期时间
        System.out.println(localDateTime.format(DateTimeFormatter.ofPattern("yyyy年MMMMd日 E hh:mm a", Locale.CHINA)));
        // String转Date
        String dateStr = "2021年一月6日 星期三 20:38 下午";
        LocalDateTime parse = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyy年MMMMd日 E HH:mm a", Locale.CHINA));
        Date toDate = Date.from(parse.atZone(ZoneId.systemDefault()).toInstant());
        System.out.println(toDate);
    }
}
