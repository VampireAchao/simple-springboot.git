package com.ruben;

/**
 * 单例
 */
public class Singleton {
    private Singleton() {
    }

    public static Singleton getInstance() {
        return Inner.instance;
    }

    private static class Inner {
        private static final Singleton instance = new Singleton();
    }
}
