package com.ruben;

import lombok.SneakyThrows;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 我还没有写描述
 *
 * @author <achao1441470436@gmail.com>
 * @date 2021/4/30 0030 11:38
 */
public class CharsetDemo {
    private static final String MY_WORDS = "阿巴阿巴阿巴阿巴";

    @SneakyThrows
    public static void main(String[] args) {
        String encode = URLEncoder.encode(MY_WORDS, StandardCharsets.UTF_8.name());
    }
}
