package com.ruben;/**
 * @ClassName: AtomicDemo
 * @Date: 2020/11/13 0013 20:48
 * @Description:
 */

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntBinaryOperator;

/**
 * @ClassName: AtomicDemo
 * @Description: 我还没有写描述
 * @Date: 2020/11/13 0013 20:48
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class AtomicDemo {
    public static void main(String[] args) {
        // 创建一个新的AtomicInteger，初始值为 0
        AtomicInteger one = new AtomicInteger();  // 0
        // 用给定的初始值创建一个新的AtomicInteger。
        AtomicInteger two = new AtomicInteger(2); // 2
        // 设置为给定值
        one.set(1);     // 1
        // 获取当前值
        System.out.println(one.get());
        // 最终设定为给定值，多个线程共享变量场景下使用
        two.lazySet(2);     // 2
        // 将原子设置为给定值并返回旧值
        one.getAndSet(5);       // 1
        // 如果当前值为==为预期值，则将该值原子设置为给定的更新值
        one.compareAndSet(5, 1);     // return true,one.get() == 1
        one.compareAndSet(8, 0);     // return false,one.get() == 1
        // 和上一个差不多，但在多线程下
        one.weakCompareAndSet(1, 2);     //
        System.out.println();
    }
}
