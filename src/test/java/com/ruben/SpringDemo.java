package com.ruben;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;

/**
 * @ClassName: SpringDemo
 * @Description: 我还没有写描述
 * @Date: 2021/2/24 0024 21:24
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@SpringBootTest
@DependsOn("SpringContextHolder")
public class SpringDemo {

    @Test
    public void test() {

    }

}
