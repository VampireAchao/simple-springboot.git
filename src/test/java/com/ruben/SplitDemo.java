package com.ruben;

/**
 * @ClassName: SplitDemo
 * @Description:
 * @Date: 2020/8/22 18:48
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class SplitDemo {
    public static void main(String[] args) {
        int i = Integer.bitCount(3);
        System.out.println(i);
    }

    private static void split() {
        String str = "ruben";
        String[] array = str.split("");
        for (String s : array) {
            System.out.println(s);
        }
        str = String.join("", array);
        System.out.println(str);
    }
}
