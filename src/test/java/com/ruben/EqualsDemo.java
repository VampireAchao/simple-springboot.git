package com.ruben;

import org.junit.Assert;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * 我还没有写描述
 *
 * @author <achao1441470436@gmail.com>
 * @date 2021/4/30 0030 11:17
 */
public class EqualsDemo {
    public static void main(String[] args) {
        String ruben = "ruben";
        String rabbit = "ruben";
        String achao = "achao";
        // 常用的Object.equals
        System.out.println(ruben.equals(rabbit));
        // 避免空指针的java.util.Objects.equals
        System.out.println(Objects.equals(ruben, rabbit));
        // 比较数组的Objects.deepEquals
        int[] rainbowNumber = new int[]{1, 2, 3, 4, 5, 6, 7};
        int[] weekNumber = new int[]{1, 2, 3, 4, 5, 6, 7};
        int[] misdaNumber = new int[]{4, 4, 4, 4};
        System.out.println(Objects.deepEquals(rainbowNumber, weekNumber));
        System.out.println(Objects.deepEquals(rainbowNumber, misdaNumber));
        // Junit的org.junit.Assert.assertEquals(java.lang.Object, java.lang.Object)，用于测试，如果equals结果为false则抛出Error
        Assert.assertEquals(rabbit, ruben);
        // java.util.function.Predicate花式写法
        System.out.println(Predicate.isEqual(ruben).test(achao));
    }
}
