package com.ruben.leetCode;/**
 * @ClassName: MaxProfitSolution
 * @Date: 2020/11/8 0008 15:47
 * @Description:
 */

import java.util.Arrays;

/**
 * @ClassName: MaxProfitSolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/8 0008 15:47
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class MaxProfitSolution {

    public static void main(String[] args) {
        MaxProfitSolution solution = new MaxProfitSolution();
        System.out.println(solution.maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
    }

    public int maxProfit(int[] prices) {
        int n = prices.length;
        int[][] dp = new int[n][2];
        dp[0][0] = 0;
        dp[0][1] = -prices[0];
        for (int i = 1; i < n; ++i) {
            dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] + prices[i]);
            dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
        }
        return dp[n - 1][0];
    }

}
