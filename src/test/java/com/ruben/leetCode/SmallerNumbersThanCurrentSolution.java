package com.ruben.leetCode;/**
 * @ClassName: SmallerNumbersThanCurrentSolution
 * @Date: 2020/10/26 0026 20:09
 * @Description:
 */

/**
 * @ClassName: SmallerNumbersThanCurrentSolution
 * @Description: 我还没有写描述
 * @Date: 2020/10/26 0026 20:09
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class SmallerNumbersThanCurrentSolution {

    public static void main(String[] args) {
        SmallerNumbersThanCurrentSolution solution = new SmallerNumbersThanCurrentSolution();
        int[] results = solution.smallerNumbersThanCurrent(new int[]{8, 1, 2, 2, 3});
        for (int result : results) {
            System.out.println(result);
        }
    }

    public int[] smallerNumbersThanCurrent(int[] nums) {
        int[] temps = nums;
        int[] results = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            results[i] = 0;
            for (int j = 0; j < temps.length; j++) {
                if (nums[i] > temps[j]) {
                    results[i]++;
                }
            }
        }
        return results;
    }
}
