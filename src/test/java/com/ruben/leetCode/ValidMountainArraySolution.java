package com.ruben.leetCode;/**
 * @ClassName: ValidMountainArraySolution
 * @Date: 2020/11/3 0003 20:13
 * @Description:
 */

import com.ruben.FastJsonDemo;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @ClassName: ValidMountainArraySolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/3 0003 20:13
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class ValidMountainArraySolution {
    public static void main(String[] args) {
        ValidMountainArraySolution solution = new ValidMountainArraySolution();
//        System.out.println(solution.validMountainArray(new int[]{2, 1}));
//        System.out.println(solution.validMountainArray(new int[]{3, 5, 5}));
        System.out.println(solution.validMountainArray(new int[]{0, 3, 2, 1}));
//        System.out.println(solution.validMountainArray(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}));
//        System.out.println(solution.validMountainArray(new int[]{0, 1, 2, 4, 2, 1}));
//        System.out.println(solution.validMountainArray(new int[]{0, 1, 2, 1, 2}));
    }


    public boolean validMountainArray(int[] A) {
        // 如果长度小于3
        if (A.length < 3) {
            // 那就不是我们的山脉数组*0
            return false;
        }
        // 拿到山顶坐标，这里boxed是装箱的意思，(int -> Integer)，indexOf里面是拿到这里最大值
        int topIndex = Arrays.stream(A).boxed().collect(Collectors.toList()).indexOf(Arrays.stream(A).summaryStatistics().getMax());
        // 判断山顶坐标是否在头或者尾
        if (topIndex == 0 || topIndex == A.length - 1) {
            // 那就不是我们的山脉数组*1
            return false;
        }
        // 定义一个坐标，这里等于for i循环
        AtomicInteger index = new AtomicInteger(1);
        // 定义一个最后结果
        AtomicBoolean result = new AtomicBoolean(true);
        // forEach循环
        Arrays.stream(A).boxed().forEach(fontInt -> {
            if (index.get() < A.length) {
                Integer backInt = A[index.get()];
                // 如果途中两个相等
                if (Objects.equals(fontInt, backInt)) {
                    // 那就不是我们的山脉数组*2
                    result.set(false);
                }
                // 如果在山顶左方并且不是 递增（前面的数小于后面的数）
                if (index.get() < topIndex && fontInt > backInt) {
                    // 那就不是我们的山脉数组*3
                    result.set(false);
                }
                // 如果在山顶右方并且不是 递减（前面的数大于后面的数）
                if (index.get() > topIndex && fontInt < backInt) {
                    // 那就不是我们的山脉数组*4
                    result.set(false);
                }
            }
            // 坐标自增
            index.getAndIncrement();
        });
        // 返回结果
        return result.get();
    }
}
