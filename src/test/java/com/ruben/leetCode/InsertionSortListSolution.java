package com.ruben.leetCode;/**
 * @ClassName: InsertionSortListSolution
 * @Date: 2020/11/20 0020 22:16
 * @Description:
 */

import com.ruben.ListNodeDemo;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * @ClassName: InsertionSortListSolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/20 0020 22:16
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class InsertionSortListSolution {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(4);
        ListNode listNode1 = new ListNode(2);
        ListNode listNode2 = new ListNode(1);
        listNode2.next = new ListNode(3);
        listNode1.next = listNode2;
        listNode.next = listNode1;
        InsertionSortListSolution solution = new InsertionSortListSolution();
        ListNode answer = solution.insertionSortList(listNode);
        System.out.println(answer.val);
    }

    public ListNode insertionSortList(ListNode head) {
        ListNode tmp = head;
        int count = 0;
        while (tmp != null) {
            count++;
            tmp = tmp.next;
        }
        return Stream.iterate(head, listNode -> listNode.next).limit(count).mapToInt(l -> l.val).sorted().collect(() -> new ListNode(0), (l, i) -> {
            ListNode tmp1 = l;
            while (tmp1.next != null) {
                tmp1 = tmp1.next;
            }
            tmp1.next = new ListNode(i);
        }, (l, l1) -> Function.identity()).next;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

}
