package com.ruben.leetCode;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @ClassName: IsAnagramSolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/22 0022 15:18
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class IsAnagramSolution {
    public static void main(String[] args) {
        IsAnagramSolution solution = new IsAnagramSolution();
        System.out.println(solution.isAnagram("anagram", "nagaram"));
    }

    public boolean isAnagram(String s, String t) {
        return Arrays.stream(s.split("")).sorted().collect(Collectors.joining()).equals(Arrays.stream(t.split("")).sorted().collect(Collectors.joining()));
    }
}
