package com.ruben.leetCode;/**
 * @ClassName: UniqueOccurrencesSolution
 * @Date: 2020/10/28 0028 20:51
 * @Description:
 */

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName: UniqueOccurrencesSolution
 * @Description: 我还没有写描述
 * @Date: 2020/10/28 0028 20:51
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class UniqueOccurrencesSolution {

    public static void main(String[] args) {
        UniqueOccurrencesSolution solution = new UniqueOccurrencesSolution();
        System.out.println(solution.uniqueOccurrences(new int[]{1, 2}));
    }

    public boolean uniqueOccurrences(int[] arr) {
        Map<Integer, Integer> countMap = new HashMap<>(1 << 4);
        Arrays.stream(arr).forEachOrdered(data -> countMap.put(data, new AtomicInteger(Optional.ofNullable(countMap.get(data)).orElse(0)).incrementAndGet()));
        return countMap.values().stream().distinct().count() == countMap.size();
    }
}
