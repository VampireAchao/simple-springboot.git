package com.ruben.leetCode;/**
 * @ClassName: IntersectionSolution
 * @Date: 2020/11/2 0002 21:14
 * @Description:
 */

import java.util.Arrays;

/**
 * @ClassName: IntersectionSolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/2 0002 21:14
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class IntersectionSolution {
    public static void main(String[] args) {
        IntersectionSolution solution = new IntersectionSolution();
        int[] intersection = solution.intersection(new int[]{1, 2, 2, 1}, new int[]{2, 2});
        for (int i : intersection) {
            System.out.println(i);
        }
    }

    public int[] intersection(int[] nums1, int[] nums2) {
        return Arrays.stream(nums1).flatMap(num1 -> Arrays.stream(nums2).filter(num2 -> num1 == num2)).distinct().toArray();
    }
}
