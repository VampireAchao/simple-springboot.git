package com.ruben.leetCode;/**
 * @ClassName: PreorderTraversalSolution
 * @Date: 2020/10/27 0027 22:54
 * @Description:
 */

import com.ruben.leetCode.PreorderTraversalSolution.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: PreorderTraversalSolution
 * @Description: 我还没有写描述
 * @Date: 2020/10/27 0027 22:54
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class PreorderTraversalSolution {

    public static void main(String[] args) {
        PreorderTraversalSolution solution = new PreorderTraversalSolution();
        TreeNode right = new TreeNode(2);
        right.right = new TreeNode(3);
        TreeNode treeNode = new TreeNode();
        treeNode.right = right;
        treeNode.left = new TreeNode(1);
        solution.preorderTraversal(treeNode).forEach(System.out::println);
    }

    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        preorder(root, res);
        return res;
    }

    public void preorder(TreeNode root, List<Integer> res) {
        if (root == null) {
            return;
        }
        res.add(root.val);
        preorder(root.left, res);
        preorder(root.right, res);
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

}
