package com.ruben.leetCode;/**
 * @ClassName: IsLongPressedNameSolution
 * @Date: 2020/10/21 0021 20:53
 * @Description:
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UnknownFormatConversionException;
import java.util.stream.Collectors;

/**
 * @ClassName: IsLongPressedNameSolution
 * @Description: 我还没有写描述
 * @Date: 2020/10/21 0021 20:53
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class IsLongPressedNameSolution {
    public static void main(String[] args) {
        IsLongPressedNameSolution solution = new IsLongPressedNameSolution();
        System.out.println(solution.isLongPressedName("saeed", "ssaaedd"));
    }

    //    public boolean isLongPressedName(String name, String typed) {
//        List<String> nameList = new ArrayList<>(Arrays.asList(name.split("")));
//        List<String> typedList = new ArrayList<>(Arrays.asList(typed.split("")));
//        String collect = nameList.stream().flatMap(n -> typedList.stream().filter(t -> t.equals(n)).distinct()).collect(Collectors.joining());
//        return name.equals(collect);
//    }
    public boolean isLongPressedName(String name, String typed) {
        int i = 0, j = 0;
        while (j < typed.length()) {
            // 遍历整个type,当j加到typed长度的时候停止
            if (i < name.length() && name.charAt(i) == typed.charAt(j)) {
                // 如果name的当前字符和type当前字符相同，i和j各加一
                i++;
                j++;
            } else if (j > 0 && typed.charAt(j) == typed.charAt(j - 1)) {
                // 或者 长按导致typed当前字符和前一个字符相等，此时只有j加一
                j++;
            } else {
                return false;
            }
        }
        return i == name.length();
    }
}
