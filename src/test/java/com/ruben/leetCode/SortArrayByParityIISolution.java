package com.ruben.leetCode;/**
 * @ClassName: sortArrayByParityIISolution
 * @Date: 2020/11/12 0012 22:05
 * @Description:
 */

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @ClassName: SortArrayByParityIISolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/12 0012 22:05
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class SortArrayByParityIISolution {
    public static void main(String[] args) {
        SortArrayByParityIISolution solution = new SortArrayByParityIISolution();
        Arrays.stream(solution.sortArrayByParityII(new int[]{1, 4, 4, 3, 0, 3})).forEach(System.out::println);
    }

    public int[] sortArrayByParityII(int[] A) {
        AtomicBoolean flag = new AtomicBoolean(false);
        AtomicInteger index = new AtomicInteger(0);
        Map<Boolean, List<Integer>> collect = Arrays.stream(A).boxed().collect(Collectors.groupingBy(i -> i % 2 == 0));
        return Arrays.stream(A).map(i -> flag.getAndSet(!flag.get()) ? collect.get(flag.get()).get(index.getAndIncrement()) : collect.get(flag.get()).get(index.get())).toArray();
    }
}
