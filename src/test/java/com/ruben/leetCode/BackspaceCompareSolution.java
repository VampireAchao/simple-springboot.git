package com.ruben.leetCode;/**
 * @ClassName: BackspaceCompare
 * @Date: 2020/10/19 0019 19:52
 * @Description:
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: BackspaceCompare
 * @Description: 我还没有写描述
 * @Date: 2020/10/19 0019 19:52
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class BackspaceCompareSolution {
    public static void main(String[] args) {
        BackspaceCompareSolution backspaceCompare = new BackspaceCompareSolution();
        System.out.println(backspaceCompare.backspaceCompare("y#f#o##f", "y#fo##f"));
    }

    public boolean backspaceCompare(String S, String T) {
        return backspace(S).equals(backspace(T));
    }

    public String backspace(String str) {
        List<String> list = Arrays.asList(str.split(""));
        List<String> list1 = new ArrayList<>();
        list.forEach(s -> {
            if (s.equals("#") && list1.size() > 0) {
                list1.remove(list1.size() - 1);
            } else {
                list1.add(s);
            }
        });
        return list1.stream().filter(s -> !s.equals("#")).collect(Collectors.joining());
    }

}
