package com.ruben.leetCode;/**
 * @ClassName: IsPalindromeSolution
 * @Date: 2020/10/23 0023 21:07
 * @Description:
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName: IsPalindromeSolution
 * @Description: 我还没有写描述
 * @Date: 2020/10/23 0023 21:07
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class IsPalindromeSolution {

    public static void main(String[] args) {
        IsPalindromeSolution solution = new IsPalindromeSolution();
        ListNode listNode = new ListNode(1);
        ListNode listNode1 = new ListNode(0);
        listNode1.next = new ListNode(0);
        listNode.next = listNode1;
        System.out.println(solution.isPalindrome(listNode));
    }

    public boolean isPalindrome(ListNode head) {
        ListNode listNode = head;
        ArrayList<Integer> list = new ArrayList<>(10);
        while (listNode != null) {
            list.add(listNode.val);
            listNode = listNode.next;
        }
        if (list.isEmpty()) {
            return true;
        }
        int length;
        if (list.size() % 2 == 0) {
            length = list.size() / 2;
        } else {
            length = (list.size() - 1) / 2;
        }
        List<Integer> reverseList = new ArrayList<>(10);
        reverseList.addAll(list);
        Collections.reverse(reverseList);
        boolean result = true;
        for (int i = 0; i < length; i++) {
            if (!list.get(i).equals(reverseList.get(i))) {
                result = false;
            }
        }
        return result;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
