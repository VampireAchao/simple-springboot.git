package com.ruben.leetCode;/**
 * @ClassName: ReorderListSolution
 * @Date: 2020/10/20 0020 21:15
 * @Description:
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: ReorderListSolution
 * @Description: 我还没有写描述
 * @Date: 2020/10/20 0020 21:15
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class ReorderListSolution {
    public void reorderList(ListNode head) {
        // 安全校验
        if (head == null) {
            return;
        }
        // 把链表转换为list
        List<ListNode> list = new ArrayList<>();
        ListNode node = head;
        while (node != null) {
            // 添加当前节点
            list.add(node);
            // 把末尾节点赋值给当前节点
            node = node.next;
        }
        // 然后进行处理
        int i = 0, j = list.size() - 1;
        while (i < j) {
            // 把list.get(j)放到list.get(i)的末尾
            list.get(i).next = list.get(j);
            // i逐渐增大
            i++;
            if (i == j) {
                break;
            }
            // 把list.get(i)放到list(j)的末尾
            list.get(j).next = list.get(i);
            // j逐渐减小
            j--;
        }
        // 最后别忘清空末尾节点
        list.get(i).next = null;
    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

}
