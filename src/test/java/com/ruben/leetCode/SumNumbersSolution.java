package com.ruben.leetCode;/**
 * @ClassName: SumNumbersSolution
 * @Date: 2020/10/29 0029 20:38
 * @Description:
 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @ClassName: SumNumbersSolution
 * @Description: 我还没有写描述
 * @Date: 2020/10/29 0029 20:38
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class SumNumbersSolution {
    private final List<String> list = new ArrayList<>(10);
    private final AtomicInteger result = new AtomicInteger(0);

    public static void main(String[] args) {
        SumNumbersSolution solution = new SumNumbersSolution();
//        TreeNode treeNode = new TreeNode(4);
//        treeNode.left = new TreeNode(9);
//        treeNode.right = new TreeNode(0);
//        treeNode.left.left = new TreeNode(5);
//        treeNode.left.right = new TreeNode(1);
//        TreeNode treeNode = new TreeNode(1);
//        treeNode.left = new TreeNode(2);
//        treeNode.right = new TreeNode(3);
        TreeNode treeNode = new TreeNode(1);
        treeNode.left = new TreeNode(0);
        System.out.println(solution.sumNumbers(treeNode));
    }

    public int sumNumbers(TreeNode root) {
        if (root == null) {
            return 0;
        }
        list.add(String.valueOf(root.val));
        sumNumbers(root.left);
        if (root.left == null && root.right == null) {
            if (!list.isEmpty()) {
                result.addAndGet(Integer.parseInt(String.join("", list)));
            }
        }
        sumNumbers(root.right);
        list.remove(list.size() - 1);
        return result.get();
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
