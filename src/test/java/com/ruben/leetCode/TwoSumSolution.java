package com.ruben.leetCode;/**
 * @ClassName: TwoSum
 * @Date: 2020/11/15 0015 22:29
 * @Description:
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName: TwoSum
 * @Description: 我还没有写描述
 * @Date: 2020/11/15 0015 22:29
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class TwoSumSolution {
    /**
     * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
     * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
     */
    public static void main(String[] args) {
        TwoSumSolution solution = new TwoSumSolution();
//        Arrays.stream(solution.twoSum(new int[]{2, 7, 11, 15}, 9)).forEach(System.out::println);
        Arrays.stream(solution.twoSum(new int[]{3, 2, 4}, 6)).forEach(System.out::println);
    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>(1 << 4);
        for (int i = 0; i < nums.length; ++i) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{map.get(target - nums[i]), i};
            }
            map.put(nums[i], i);
        }
        return new int[0];
    }


}
