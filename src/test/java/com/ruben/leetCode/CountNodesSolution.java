package com.ruben.leetCode;

import java.util.List;

/**
 * @ClassName: CountNodesSolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/24 0024 20:19
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class CountNodesSolution {
    int res = 0;

    public static void main(String[] args) {
        CountNodesSolution solution = new CountNodesSolution();
        TreeNode one = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        TreeNode three = new TreeNode(3);
        two.left = new TreeNode(4);
        two.right = new TreeNode(5);
        three.right = new TreeNode(6);
        one.left = two;
        one.right = three;
        System.out.println(solution.countNodes(one));
    }

    public int countNodes(TreeNode root) {
        if (root == null) {
            return 0;
        }
        res++;
        countNodes(root.left);
        countNodes(root.right);
        return res;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
