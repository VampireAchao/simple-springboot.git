package com.ruben.leetCode;/**
 * @ClassName: ReverseSolution
 * @Date: 2020/11/15 0015 22:58
 * @Description:
 */

import lombok.Builder;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: ReverseSolution
 * @Description: 我还没有写描述
 * @Date: 2020/11/15 0015 22:58
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Builder
public class ReverseSolution {
    public static void main(String[] args) {
        ReverseSolution build = ReverseSolution.builder().build();
//        System.out.println(build.reverse(1534236469));
    }

    public int reverse(int x) {
        List<String> collect = Arrays.stream(String.valueOf(x).split("")).collect(Collectors.toList());
        Collections.reverse(collect);
        String join = String.join("", collect);
        if (x < 0) {
            join = "-" + join.replace("-", "");
        }
        if (Double.parseDouble(join) > Integer.MAX_VALUE || Double.parseDouble(join) < Integer.MIN_VALUE) {
            return 0;
        }
        return Integer.parseInt(join);
    }
}
