package com.ruben.controller;

import com.ruben.enumeration.GenderEnum;
import com.ruben.pojo.po.UserPO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

    @Value("${ruben.gender}")
    private GenderEnum gender;
    @Resource
    private HttpServletRequest httpServletRequest;

    @RequestMapping("/get")
    public GenderEnum get() {
        return gender;
    }


    @RequestMapping("session")
    public String session() {
        httpServletRequest.getSession().setAttribute("sessionTest", UserPO.builder().id(1).build());
        return "ok";
    }


}
