package com.ruben.controller;

import com.ruben.feign.ConsumerService;
import com.ruben.pojo.dto.PageDTO;
import com.ruben.utils.AjaxJson;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @ClassName: StudyController
 * @Description: 我还没有写描述
 * @Date: 2021/3/4 0004 21:49
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@RestController
@RequestMapping("study")
public class StudyController {
    @Resource
    private ConsumerService studyFeignService;

    @PostMapping("list")
    public AjaxJson list(@RequestBody PageDTO pageDTO) {
        return studyFeignService.list(pageDTO);
    }

    @DeleteMapping
    public AjaxJson delete(@RequestParam String id) {
        return AjaxJson.success(id);
    }

}
