package com.ruben.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruben.utils.AjaxJson;
import com.ruben.utils.Encrypt;
import com.ruben.utils.green.GreenResult;
import com.ruben.utils.green.GreenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @ClassName: GreenController
 * @Description: 鉴黄Controller
 * @Date: 2020/8/8 15:19
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Slf4j
@RestController
@RequestMapping("green")
public class GreenController {

    @Resource
    private GreenUtils greenUtils;

    /**
     * 图片鉴黄
     *
     * @param url
     * @return
     */
    @GetMapping("greenPic")
    public AjaxJson greenPic(@RequestParam String url) {
        if ("".equals(url)) {
            return greenUtils.greenPic("https://img.alicdn.com/tfs/TB19dNAmh6I8KJjy0FgXXXXzVXa-1280-1280.jpg", "https://img.alicdn.com/tfs/TB1aANXmgDD8KJjy0FdXXcjvXXa-1280-1280.jpg");
        }
        return greenUtils.greenPic(url);
    }

    /**
     * 文本反垃圾
     *
     * @param text
     * @return
     */
    @GetMapping("greenText")
    public AjaxJson greenText(@RequestParam String text) {
        if ("".equals(text)) {
            return greenUtils.greenText("棒棒哒", "爷 是 二 次 元");
        }
        return greenUtils.greenText(text);
    }

    /**
     * 异步视频鉴黄
     *
     * @param url
     * @return
     */
    @GetMapping("greenVideo")
    public AjaxJson greenVideo(@RequestParam String url) {
        if ("".equals(url)) {
            return greenUtils.greenVideo("https://waibi.oss-cn-chengdu.aliyuncs.com/QQ%E8%A7%86%E9%A2%91_0670c7201774824ee71c7e2fe4c0dc4d1586480746.mp4", "https://waibi.oss-cn-chengdu.aliyuncs.com/QQ%E8%A7%86%E9%A2%91_b5ca71298bf92a778ef1ce94259dcdce1580718352.mp4");
        }
        return greenUtils.greenVideo(url);
    }

    /**
     * 查询异步视频鉴黄结果
     *
     * @param taskId
     * @return
     */
    @GetMapping("checkVideo")
    public AjaxJson checkVideo(@RequestParam String taskId) {
        return greenUtils.checkVideo(taskId);
    }

    /**
     * 异步视频鉴黄回调
     *
     * @param greenResult
     */
    @PostMapping("videoCallback")
    public void videoCallback(GreenResult greenResult) {
        /**
         *uid + seed + content
         */
        if (Encrypt.SHA256("1782361992781435" + "ruben" + greenResult.getContent()).equals(greenResult.getChecksum())) {
            JSONObject jsonObject = JSONObject.parseObject(greenResult.getContent());
            System.out.println(JSON.toJSONString(jsonObject, true));
            JSONArray scenes = jsonObject.getJSONArray("scenes");
            JSONArray audioScenes = jsonObject.getJSONArray("audioScenes");
            JSONArray taskResults = jsonObject.getJSONArray("results");
            for (Object taskResult : taskResults) {
                String dataId = ((JSONObject) taskResult).getString("dataId");
            }
            //回调逻辑
        } else {
            log.error("返回参数被篡改或配置有误");
        }
    }


}
