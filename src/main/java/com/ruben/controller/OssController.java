package com.ruben.controller;/**
 * @ClassName: OssController
 * @Date: 2020/11/21 0021 15:15
 * @Description:
 */

import com.ruben.utils.AjaxJson;
import com.ruben.utils.AliyunUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: OssController
 * @Description: 我还没有写描述
 * @Date: 2020/11/21 0021 15:15
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@RestController
@RequestMapping("oss")
public class OssController {
    /**
     * @MethodName: getMark
     * @Description: 获取OSS签证
     * @Date: 2020/11/21 0021 15:15
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: []
     * @returnValue: com.ruben.utils.AjaxJson
     */
    @GetMapping("getMark")
    public AjaxJson getMark() {
        return AliyunUtils.getOssMark();
    }
}
