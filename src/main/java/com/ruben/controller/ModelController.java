package com.ruben.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: ModelController
 * @Description: 我还没有写描述
 * @Date: 2020/12/23 0023 20:36
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@RestController
@RequestMapping("model")
public class ModelController {

    @ModelAttribute
    public String getWords() {
        return "ruben";
    }

    @GetMapping("whoIsAuthor")
    public String whoIsAuthor(@ModelAttribute String word) {
        return word;
    }

}
