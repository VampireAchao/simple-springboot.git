package com.ruben.resolver;

import lombok.SneakyThrows;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.swing.text.DateFormatter;
import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;

/**
 * 全局日期转换处理器
 *
 * @author <achao1441470436@gmail.com>
 * @date 2021/4/5 0005 0:45
 */
@RestControllerAdvice
public class GlobalTimeResolver {
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);
    public static final ZoneId SYSTEM_DEFAULT_ZONE_ID = ZoneId.systemDefault();

    /**
     * 我还没有写描述
     *
     * @param binder 从Web请求参数到JavaBean对象的数据绑定的特殊DataBinder
     * @author <achao1441470436@gmail.com>
     * @date 2021/4/5 0005 0:48
     */
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            @SneakyThrows
            public void setAsText(String text) {
                setValue(textToDate(text));
            }
        });
        // LocalDate类型转换
        binder.registerCustomEditor(LocalDate.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(textToLocalDate(text));
            }
        });
        // LocalDateTime类型转换
        binder.registerCustomEditor(LocalDateTime.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(textToLocalDateTime(text));
            }
        });
    }

    /**
     * 时间字符串转Date
     *
     * @param text 2021-04-04 23:10:51
     * @return java.util.Date|null
     * @author <achao1441470436@gmail.com>
     * @date 2021/4/4 0004 23:10
     */
    private Date textToDate(String text) {
        return Optional.ofNullable(textToLocalDateTime(text)).map(dateTime -> dateTime.atZone(SYSTEM_DEFAULT_ZONE_ID)).map(ZonedDateTime::toInstant).map(Date::from).orElse(null);
    }


    private LocalDate textToLocalDate(String text) {
        return Optional.ofNullable(text).filter(str -> !str.isEmpty()).map(str -> LocalDate.parse(str, DATE_FORMATTER)).orElse(null);
    }

    /**
     * 时间字符串转LocalDateTime
     *
     * @param text 2021-04-04 23:09:52
     * @return java.time.ZonedDateTime|null
     * @author <achao1441470436@gmail.com>
     * @date 2021/4/4 0004 23:05
     */
    private LocalDateTime textToLocalDateTime(String text) {
        return Optional.ofNullable(text).filter(str -> !str.isEmpty()).map(str -> LocalDateTime.parse(str, DATE_TIME_FORMATTER)).orElse(null);
    }


}
