package com.ruben.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @ClassName: GenderEnum
 * @Description:
 * @Date: 2020/8/18 19:03
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Getter
@AllArgsConstructor
public enum GenderEnum {

    FEMALE("女", 0),
    MALE("男", 1);

    private final String name;
    private final Integer code;

}
