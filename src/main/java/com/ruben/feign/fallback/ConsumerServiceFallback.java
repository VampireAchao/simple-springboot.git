package com.ruben.feign.fallback;

import com.ruben.feign.ConsumerService;
import com.ruben.pojo.dto.PageDTO;
import com.ruben.utils.AjaxJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName: ConsumerServiceFallback
 * @Description: 我还没有写描述
 * @Date: 2021/3/16 0016 22:14
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Slf4j
@Service
public class ConsumerServiceFallback implements ConsumerService {
    @Override
    public AjaxJson list(PageDTO pageDTO) {
        log.error("服务挂了");
        return AjaxJson.error("");
    }

    @Override
    public AjaxJson dropWare() {
        log.error("服务挂了");
        return AjaxJson.error("");
    }
}
