package com.ruben.feign;

import com.ruben.feign.fallback.ConsumerServiceFallback;
import com.ruben.pojo.dto.PageDTO;
import com.ruben.utils.AjaxJson;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "ruben-consumer", fallback = ConsumerServiceFallback.class)
public interface ConsumerService {
    @GetMapping("study/list")
    AjaxJson list(@RequestBody PageDTO pageDTO);

    @GetMapping("ware")
    AjaxJson dropWare();
}
