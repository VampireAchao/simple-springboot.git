package com.ruben.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: AliyunConfig
 * @Description:
 * @Date: 2020/8/9 11:02
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Configuration
public class AliyunConfig {

    /**
     * ALIYUN_ACCESS_KEY_ID和ALIYUN_ACCESS_KEY_SECRET中传入您自己的AccessKey信息。
     * REGION_ID可选值：cn-shanghai、cn-beijing、ap-southeast-1、us-west-1。其他地域暂不支持，请勿使用。
     * 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM子用户进行API访问或日常运维。
     */
    @Value("${spring.cloud.alicloud.access-key}")
    String ALIYUN_ACCESS_KEY_ID;
    @Value("${spring.cloud.alicloud.secret-key}")
    String ALIYUN_ACCESS_KEY_SECRET;
    @Value("${spring.cloud.alicloud.oss.endpoint}")
    String ENDPOINT;
    String REGION_ID = "cn-shanghai";

    /**
     * IAcsClient是aliyun-java-sdk-green的Java客户端。使用aliyun-java-sdk-green Java SDK发起请求前，您需要初始化一个IAcsClient实例，并根据需要修改IClientProfile的配置项。
     *
     * @return
     */
    @Bean
    public IAcsClient iAcsClient() {
        DefaultProfile profile = DefaultProfile.getProfile(REGION_ID, ALIYUN_ACCESS_KEY_ID, ALIYUN_ACCESS_KEY_SECRET);
        return new DefaultAcsClient(profile);
    }

    @Bean
    public OSS oss() {
        // 创建OSS实例。
        return new OSSClientBuilder().build(ENDPOINT, ALIYUN_ACCESS_KEY_ID, ALIYUN_ACCESS_KEY_SECRET);
    }


}
