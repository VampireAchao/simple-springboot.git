package com.ruben.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @ClassName: RabbitmqConfig
 * @Description: 我还没有写描述
 * @Date: 2021/2/19 0019 21:53
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Configuration
public class RabbitmqConfig {
    /**
     * 短信队列名称
     */
    public static final String QUEUE_RUBEN_SMS = "queue_ruben_sms";
    /**
     * 死信队列名称
     */
    public static final String QUEUE_DEAD_RUBEN_SMS = "queue_dead_ruben_sms";
    /**
     * 交换机名称
     */
    public static final String EXCHANGE_RUBEN = "exchange_ruben";
    /**
     * 死信交换机
     */
    public static final String EXCHANGE_DEAD_RUBEN = "exchange_dead_ruben";
    /**
     * 路由key
     */
    public static final String ROUTING_KEY_RUBEN = "ruben.sms";

    /**
     * 创建队列
     *
     * @return
     */
    @Bean
    public Queue smsQueue() {
        return QueueBuilder
                // 持久化队列
                .durable(QUEUE_RUBEN_SMS)
                // 绑定我们的死信交换机
                .deadLetterExchange(EXCHANGE_DEAD_RUBEN)
                // 死信路由key
                .deadLetterRoutingKey(ROUTING_KEY_RUBEN)
                // 消息默认存活10秒
                .ttl(10000)
                .build();
    }

    /**
     * 交换机
     *
     * @return
     */
    @Bean
    public FanoutExchange fanoutExchange() {
        return ExchangeBuilder.fanoutExchange(EXCHANGE_RUBEN).build();
    }

    /**
     * 绑定交换机和队列
     *
     * @param smsQueue
     * @param fanoutExchange
     * @return
     */
    @Bean
    public Binding bindingSmsFanoutExchange(Queue smsQueue, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(smsQueue).to(fanoutExchange);
    }

}
