package com.ruben.sakila.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruben.sakila.entity.Language;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021-03-30
 */
public interface ILanguageService extends IService<Language> {

}
