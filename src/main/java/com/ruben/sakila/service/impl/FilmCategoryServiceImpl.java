package com.ruben.sakila.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruben.sakila.entity.FilmCategory;
import com.ruben.sakila.mapper.FilmCategoryMapper;
import com.ruben.sakila.service.IFilmCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021-03-30
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FilmCategoryServiceImpl extends ServiceImpl<FilmCategoryMapper, FilmCategory> implements IFilmCategoryService {

}
