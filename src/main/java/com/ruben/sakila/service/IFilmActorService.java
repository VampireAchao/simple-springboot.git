package com.ruben.sakila.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruben.sakila.entity.FilmActor;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021-03-30
 */
public interface IFilmActorService extends IService<FilmActor> {

}
