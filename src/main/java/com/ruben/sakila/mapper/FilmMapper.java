package com.ruben.sakila.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruben.sakila.entity.Film;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021-03-30
 */
public interface FilmMapper extends BaseMapper<Film> {

    List<Map<String, Object>> selectFilmAndLanguage();
}
