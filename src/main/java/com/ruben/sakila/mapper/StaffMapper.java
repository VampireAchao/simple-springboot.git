package com.ruben.sakila.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruben.sakila.entity.Staff;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021-03-30
 */
public interface StaffMapper extends BaseMapper<Staff> {

}
