package com.ruben.utils;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName: StringUtil
 * @Description:
 * @Date: 2020/8/10 21:22
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class StringUtil {
    /**
     * @param initial  初始字符串
     * @param interval 分段长度
     * @return
     */
    public static List<String> stringSplit(String initial, Integer interval) {
        List<String> result = new LinkedList<>();
        StringBuilder tmp = new StringBuilder(initial);
        int length = tmp.length();
        while (length > 0) {
            if (interval > length) {
                interval = length;
            }
            String tmpStr = tmp.substring(0, interval);
            result.add(tmpStr);
            tmp.replace(0, interval, "");
            length = tmp.length();
        }
        return result;
    }
}
