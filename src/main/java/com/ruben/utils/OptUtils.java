package com.ruben.utils;


import com.ruben.pojo.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

/**
 * @ClassName: OptUtils
 * @Date: 2020/11/25 0025 15:02
 * @Description: Optional工具类避免空指针
 * @Author: <achao1441470436@gmail.com>
 */
public class OptUtils {
    /**
     * @MethodName: toStripZeroString
     * @Description: 转换成去零字符串
     * @Date: 2020/11/25 0025 15:04
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [bigDecimal]
     * @returnValue: java.lang.String
     */
    public static String toStripZeroString(BigDecimal bigDecimal) {
        return Optional.ofNullable(bigDecimal).map(BigDecimal::stripTrailingZeros).map(BigDecimal::toPlainString).orElse("0");
    }

    public static String toStripZeroString(AtomicReference<BigDecimal> bigDecimalAtomicReference) {
        return toStripZeroString(bigDecimalAtomicReference.get());
    }

    /**
     * @MethodName: nullToNew
     * @Description: 如果为空则new一个，避免空指针
     * @Date: 2020/11/25 0025 15:19
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [obj, cls]
     * @returnValue: Object
     */
    public static Object nullToNew(Object obj, Class cls) {
        try {
            return Optional.ofNullable(obj).orElse(cls.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @MethodName: listGetFirst
     * @Description: list获取第一个
     * @Date: 2020/11/25 0025 15:54
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [list, cls]
     * @returnValue: java.lang.Object
     */
    @SuppressWarnings("unchecked")
    public static Object listGetFirst(List list, Class cls) {
        try {
            return Optional.ofNullable(list).orElse(new ArrayList()).stream().findFirst().orElse(cls.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @MethodName: getNotNull
     * @Description: 空指针处理
     * @Date: 2021/1/3 0003 15:39
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [handler]
     * @returnValue: java.util.Optional<T>
     */
    public static <T> Optional<T> getNotNull(Supplier<T> handler) {
        try {
            T result = handler.get();
            return Optional.ofNullable(result);
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    /**
     * @MethodName: getListNotNull
     * @Description: 数组越界、空指针优雅处理
     * @Date: 2021/1/3 0003 15:39
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [list, index]
     * @returnValue: java.util.Optional<T>
     */
    public static <T> Optional<T> getListNotNull(List<T> list, int index) {
        try {
            return Optional.ofNullable(list.get(index));
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    /**
     * @MethodName: getArrayNotNull
     * @Description: 数组越界、空指针优雅处理
     * @Date: 2021/1/3 0003 15:35
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [array, index]
     * @returnValue: java.util.Optional<T>
     */
    public static <T> Optional<T> getArrayNotNull(T[] array, int index) {
        try {
            return Optional.ofNullable(array[index]);
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    /**
     * @MethodName: main
     * @Description: 测试用例
     * @Date: 2020/11/25 0025 15:36
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [args]
     * @returnValue: void
     */
    public static void main(String[] args) {
        System.out.println(toStripZeroString(new BigDecimal("22.5")));
        User user = (User) nullToNew(null, User.class);
//        System.out.println(user.getId());
        List<User> users = new ArrayList<>();
        System.out.println(users);
        Object o = listGetFirst(users, User.class);
        System.out.println(o);
    }

}
