package com.ruben.utils.server;

import lombok.Data;
import oshi.hardware.CentralProcessor;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

@Data
public class ServerInfo implements Serializable {
    private static final long serialVersionUID = 3370922003623989129L;
    private Jvm jvm;
    private String hostname;
    private List<FileItem> file;
    private MemoryInfo mem;
    private String ip;
    private Cpu cpu;
    private Properties sys;

    @Data
    public static class MemoryInfo implements Serializable {
        private static final long serialVersionUID = 6743505973159689011L;
        private Double total;
        private Double used;
        private Double usedMem;
        private Double free;
    }

    @Data
    public static class Cpu implements Serializable {
        private static final long serialVersionUID = 8497412342374160092L;
        private Integer num;
        private Double used;
        private Double free;
        private CentralProcessor cpucard;
    }

    @Data
    public static class FileItem implements Serializable {
        private static final long serialVersionUID = 6046898710554975526L;
        private String path;
        private String total;
        private Double rate;
        private String name;
        private String used;
        private String type;
        private String free;
    }

    @Data
    public static class Jvm implements Serializable {
        private static final long serialVersionUID = 7530536143668960080L;
        private Double total;
        private Double maxTotal;
        private Double used;
        private Double usedMem;
    }
}
