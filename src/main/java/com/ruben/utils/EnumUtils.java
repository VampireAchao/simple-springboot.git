package com.ruben.utils;/**
 * @ClassName: EnumUtils
 * @Date: 2020/11/15 0015 21:35
 * @Description:
 */

import com.ruben.enumeration.GenderEnum;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.ruben.enumeration.GenderEnum.MALE;

/**
 * @ClassName: EnumUtils
 * @Description: 我还没有写描述
 * @Date: 2020/11/15 0015 21:35
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class EnumUtils {

    private static final String VALUE_OF = "valueOf";

    public static Object getMethodReturnByName(GenderEnum[] genderEnum, String nameInDatabase, String methodName) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        GenderEnum anEnum = genderEnum[0];
        Class<? extends GenderEnum> aClass = anEnum.getClass();
        Method valueOf = aClass.getMethod(VALUE_OF, String.class);
        Object invoke = valueOf.invoke(genderEnum, nameInDatabase);
        Class<?> invokeClass = invoke.getClass();
        Method getCode = invokeClass.getMethod(methodName);
        return getCode.invoke(invoke);
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Integer code = (Integer) getMethodReturnByName(GenderEnum.values(), "FEMALE", "getCode");
        System.out.println(code);
    }
}
