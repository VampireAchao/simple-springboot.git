package com.ruben.utils.green;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: GreenResult
 * @Description:
 * @Date: 2020/8/9 10:58
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GreenResult {
    /**
     * 指定反馈的场景。可选值包括：
     * porn：图片智能鉴黄
     * terrorism：图片暴恐涉政识别
     * ad：图文违规识别
     * qrcode：图片二维码识别
     * live：图片不良场景识别
     * logo：图片logo识别
     */
    private String scene;
    /**
     * 命中场景
     * porn
     * normal：正常图片，无色情内容
     * sexy：性感图片
     * porn：色情图片
     * <p>
     * terrorism
     * normal：正常图片
     * bloody：血腥
     * explosion：爆炸烟光
     * outfit：特殊装束
     * logo：特殊标识
     * weapon：武器
     * politics：涉政
     * violence ： 打斗
     * crowd：聚众
     * parade：游行
     * carcrash：车祸现场
     * flag：旗帜
     * location：地标
     * others：其他
     * <p>
     * ad
     * normal：正常图片
     * politics：文字含涉政内容
     * porn：文字含涉黄内容
     * abuse：文字含辱骂内容
     * terrorism：文字含暴恐内容
     * contraband：文字含违禁内容
     * spam：文字含其他垃圾内容
     * npx：牛皮藓广告
     * qrcode：包含二维码
     * programCode：包含小程序码
     * ad：其他广告
     * <p>
     * qrcode
     * normal：正常图片
     * qrcode：含二维码的图片
     * programCode：含小程序码的图片
     * <p>
     * live
     * normal：正常图片
     * meaningless：无意义图片
     * PIP：画中画
     * smoking：吸烟
     * drivelive：车内直播
     * <p>
     * logo
     * normal：正常图片
     * TV：带有管控logo的图片
     * trademark：商标
     */
    private String label;
    /**
     * pass：正常
     * review：人工审核
     * block：违规
     */
    private String suggestion;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 由用户uid + seed + content拼成字符串，通过SHA256算法生成。用户UID即阿里云账号ID，可以在阿里云控制台查询。为防篡改，您可以在获取到推送结果时，按上述算法生成字符串，与checksum做一次校验。
     */
    private String checksum;
    /**
     * 响应
     */
    private String content;
}
