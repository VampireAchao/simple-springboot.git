package com.ruben.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: CamelCaseUtils
 * @Description:
 * @Date: 2020/8/31 19:13
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class CamelCaseUtils {
    private static final Pattern underlinePattern = Pattern.compile("_(\\w)");
    private static final Pattern upperCasePattern = Pattern.compile("[A-Z]");

    public static String camelCaseToLowerCaseUnderline(String value) {
        if (value == null || "".equals(value.trim())) {
            return null;
        }
        Matcher matcher = upperCasePattern.matcher(value);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public static String lowerCaseUnderlineToCamelCase(String value) {
        if (value == null || "".equals(value.trim())) {
            return null;
        }
        value = value.toLowerCase();
        Matcher matcher = underlinePattern.matcher(value);
        StringBuffer stringBuffer = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(stringBuffer, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

    public static void main(String[] args) {
        String underlineString = camelCaseToLowerCaseUnderline("youKnowWhatHeSay");
        System.out.println(underlineString);
        String lowerCaseString = lowerCaseUnderlineToCamelCase(underlineString);
        System.out.println(lowerCaseString);
    }

}
