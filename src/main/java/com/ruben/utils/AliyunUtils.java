package com.ruben.utils;


import com.ruben.service.ApiOssService;
import com.ruben.utils.green.GreenUtils;

/**
 * @ClassName: AliyunUtils
 * @Date: 2020/11/13 0013 14:04
 * @Description: 阿里云相关都扔这
 * @Author: <achao1441470436@gmail.com>
 */
public class AliyunUtils {

    private static final ApiOssService apiOssService = SpringContextHolder.getBean(ApiOssService.class);
    private static final GreenUtils greenUtils = SpringContextHolder.getBean(GreenUtils.class);

    /**
     * @MethodName: getOssMark
     * @Description: 获取签证
     * @Date: 2020/11/13 0013 15:16
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: []
     * @returnValue: com.yth.nfp.common.json.AjaxJson
     */
    public static AjaxJson getOssMark() {
        return apiOssService.getMark();
    }

    /**
     * @MethodName: greenPic
     * @Description: 图片鉴黄
     * @Date: 2020/11/21 0021 15:09
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [url]
     * @returnValue: com.ruben.utils.AjaxJson
     */
    public static AjaxJson greenPic(String url) {
        return greenUtils.greenPic(url);
    }

    /**
     * @MethodName: greenText
     * @Description: 文本反垃圾
     * @Date: 2020/11/21 0021 15:13
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [text]
     * @returnValue: com.ruben.utils.AjaxJson
     */
    public static AjaxJson greenText(String text) {
        return greenUtils.greenText(text);
    }

    /**
     * @MethodName: greenVideo
     * @Description: 异步视频鉴黄
     * @Date: 2020/11/21 0021 15:13
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [url]
     * @returnValue: com.ruben.utils.AjaxJson
     */
    public static AjaxJson greenVideo(String url) {
        return greenUtils.greenVideo(url);
    }

    /**
     * @MethodName: checkVideo
     * @Description: 查询异步视频鉴黄结果
     * @Date: 2020/11/21 0021 15:14
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [taskId]
     * @returnValue: com.ruben.utils.AjaxJson
     */
    public static AjaxJson checkVideo(String taskId) {
        return greenUtils.checkVideo(taskId);
    }


}
