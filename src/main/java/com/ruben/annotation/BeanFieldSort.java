package com.ruben.annotation;

import java.lang.annotation.*;

/**
 * @ClassName: BeanFieldSort
 * @Description:
 * @Date: 2020/9/11 22:18
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Repeatable(BeanFieldSort.BeanFieldSorts.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BeanFieldSort {
    /**
     * 序号
     *
     * @return
     */
    int order();

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @interface BeanFieldSorts {
        BeanFieldSort[] value();
    }
}
