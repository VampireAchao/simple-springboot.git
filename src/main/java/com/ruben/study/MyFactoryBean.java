package com.ruben.study;

import com.ruben.pojo.User;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 * @ClassName: MyFactoryBean
 * @Description: 我还没有写描述
 * @Date: 2020/12/30 0030 20:40
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Component
public class MyFactoryBean implements FactoryBean {
    @Override
    public Object getObject() throws Exception {
        User user = new User();
        user.setUsername("ruben");
        return user;
    }

    @Override
    public Class<?> getObjectType() {
        return User.class;
    }
}
