package com.ruben.sql;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @ClassName: UserSqlEnum
 * @Date: 2020/10/11 0011 11:39
 * @Description:
 */
@Getter
@AllArgsConstructor
public enum UserSqlEnum {
    CREATE_SQL_LITE(
            "CREATE TABLE IF NOT EXISTS `user` (" +
                    "`id` INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "`username` text NOT NULL," +
                    "`password` text NOT NULL" +
                    ");"),
    DROP_TABLE("DROP TABLE IF EXISTS `user`;");
    private final String command;
}
