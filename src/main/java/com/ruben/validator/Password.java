package com.ruben.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotBlank;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @ClassName: Password
 * @Description:
 * @Date: 2020/8/15 16:36
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@NotBlank
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
@ReportAsSingleViolation
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
public @interface Password {
    String message() default "密码应在6-20字符之间，其中小写字母、大写字母、数字和字符必须有三种以上，不能出现连续三个重复的字符,且不能为汉字";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
