package com.ruben.validator;

import com.ruben.enumeration.GenderEnum;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @ClassName: Gender
 * @Description:
 * @Date: 2020/8/18 19:25
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@NotNull
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = {GenderValidator.class})
@ReportAsSingleViolation
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
public @interface Gender {
    String message() default "性别应该为：男1 女0";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    GenderEnum[] value();

}
