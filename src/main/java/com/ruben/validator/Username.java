package com.ruben.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @ClassName: Username
 * @Description:
 * @Date: 2020/8/15 15:39
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@NotBlank
@Documented
@Pattern(regexp = "^[A-Za-z0-9]{8,16}$")
@Retention(RUNTIME)
@Constraint(validatedBy = {})
@ReportAsSingleViolation
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
public @interface Username {
    String message() default "用户名必须满足8-16位的英文和数字";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
