package com.ruben.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruben.pojo.po.UserInfoPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName: MpUserInfoMapper
 * @Description: 我还没有写描述
 * @Date: 2021/1/23 0023 22:36
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Mapper
public interface MpUserInfoMapper extends BaseMapper<UserInfoPO> {
}
