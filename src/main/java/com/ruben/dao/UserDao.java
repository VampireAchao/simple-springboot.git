package com.ruben.dao;

import com.ruben.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName: UserDao
 * @Date: 2020/10/11 0011 13:24
 * @Description:
 */
//@Mapper
public interface UserDao {

    @SelectProvider(type = UserMapper.class, method = "findUserByCondition")
    User findUserByCondition(User user);

    @InsertProvider(type = UserMapper.class, method = "insertUser")
    int insertUser(User user);

}
