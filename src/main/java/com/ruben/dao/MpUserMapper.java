package com.ruben.dao;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruben.pojo.po.UserPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName: MpUserMapper
 * @Date: 2020/11/21 0021 15:54
 * @Description:
 */
@Mapper
public interface MpUserMapper extends BaseMapper<UserPO> {
    @SqlParser(filter = true)
    List<UserPO> findUserList(UserPO userPO);
}
