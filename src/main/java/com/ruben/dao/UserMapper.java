package com.ruben.dao;

import com.aliyuncs.utils.StringUtils;
import com.ruben.pojo.User;
import com.ruben.utils.StringUtil;
import org.apache.ibatis.jdbc.SQL;
import org.thymeleaf.expression.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: UserMapper
 * @Date: 2020/10/11 0011 13:26
 * @Description:
 */
public class UserMapper {
    public String findUserByCondition(User user) {
        SQL sql = new SQL();
        sql.SELECT("id,username,password");
        sql.FROM("user");
        if (user.getId() != null) {
            sql.WHERE("id=#{id}");
        } else if (StringUtils.isNotEmpty(user.getUsername())) {
            sql.WHERE("username=#{username}");
        }
        return sql.toString();
    }

    public String insertUser(User user) {
        SQL sql = new SQL();
        sql.INSERT_INTO("user");
        sql.VALUES("id,username,password", "#{id},#{username},#{password}");
        return sql.toString();
    }
}
