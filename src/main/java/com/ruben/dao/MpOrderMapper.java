package com.ruben.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruben.pojo.po.OrderPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MpOrderMapper extends BaseMapper<OrderPO> {
}
