package com.ruben.pojo;

import lombok.Data;

/**
 * @ClassName: BaseEntity
 * @Description: 我还没有写描述
 * @Date: 2021/1/21 0021 21:01
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
public class BaseEntity {

    protected Integer id;

    public BaseEntity() {
    }

    public BaseEntity(Integer id) {
        this();
        this.id = id;
    }

}
