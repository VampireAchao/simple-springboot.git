package com.ruben.pojo.bo;

import com.ruben.pojo.po.UserInfoPO;
import com.ruben.pojo.po.UserPO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName: UserBO
 * @Description: 我还没有写描述
 * @Date: 2021/1/23 0023 22:21
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserBO implements Serializable {
    private static final long serialVersionUID = -8368935565931556343L;
    private UserPO user;
    private UserInfoPO userInfo;
}
