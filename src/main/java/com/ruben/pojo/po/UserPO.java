package com.ruben.pojo.po;/**
 * @ClassName: UserDataObject
 * @Date: 2020/11/21 0021 15:55
 * @Description:
 */

import com.baomidou.mybatisplus.annotation.*;
import com.ruben.pojo.BaseEntity;
import com.ruben.pojo.UserInfo;
import lombok.*;

import java.io.Serializable;

/**
 * @ClassName: UserPO
 * @Description: 我还没有写描述
 * @Date: 2020/11/21 0021 15:55
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName("user")
public class UserPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -1891465370283313432L;
    @TableId(type = IdType.ID_WORKER)
    private Integer id;
    @TableField(condition = SqlCondition.LIKE)
    private String username;
    private String password;
    @TableField(exist = false)
    private UserInfo userInfo;

    public UserPO(Integer id) {
        super(id);
    }

}
