package com.ruben.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @ClassName: OrderPO
 * @Description: 我还没有写描述
 * @Date: 2021/3/13 0013 21:54
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Builder
@TableName("`order`")
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class OrderPO implements Serializable {
    private static final long serialVersionUID = 5494342842978779677L;
    /**
     * 编号
     */
    @TableId(type = IdType.INPUT)
    private Long id;
    /**
     * 发货时间
     */
    private Date sendTime;
    /**
     * 收货时间
     */
    private LocalDateTime receiveTime;
    /**
     * 保质期到期时间
     */
    private LocalDate dateProduction;
}
