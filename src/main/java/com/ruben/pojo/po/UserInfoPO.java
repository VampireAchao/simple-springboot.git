package com.ruben.pojo.po;

import com.ruben.annotation.BeanFieldSort;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName: UserInfoPO
 * @Description: 我还没有写描述
 * @Date: 2021/1/23 0023 22:34
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoPO implements Serializable {
    private static final long serialVersionUID = -4811520466138197276L;
    private Integer id;
    private String phoneNumber;
    private String address;
    private String qqNumber;
    private String wxNumber;
    private String weiboNumber;
}
