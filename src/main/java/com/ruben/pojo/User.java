package com.ruben.pojo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruben.enumeration.GenderEnum;
import com.ruben.validator.Password;
import com.ruben.validator.Username;
import lombok.*;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * @ClassName: User
 * @Description:
 * @Date: 2020/8/6 20:22
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 2436846831898074611L;
    private Integer id;
    @Username(groups = {AddCheck.class})
    private String username;
    @Password(groups = {AddCheck.class})
//    @JsonIgnore
    private String password;
    @Valid
    private UserInfo userInfo;
    //    @Gender({GenderEnum.MALE, GenderEnum.FEMALE})
    private GenderEnum genderEnum;
    private String uuid;

    public User(@Username String username, @Password String password) {
        this.username = username;
        this.password = password;
    }

    public interface AddCheck {
    }

    public interface UpdateCheck {
    }
}
