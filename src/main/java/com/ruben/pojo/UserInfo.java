package com.ruben.pojo;

import com.ruben.annotation.BeanFieldSort;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName: UserInfo
 * @Description:
 * @Date: 2020/8/15 20:05
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo implements Serializable {
    @BeanFieldSort(order = 0)
    @BeanFieldSort(order = -1)
    private static final long serialVersionUID = 3837822206443951224L;
    @BeanFieldSort(order = 5)
    private String phoneNumber;
    @BeanFieldSort(order = 4)
    private String address;
    @BeanFieldSort(order = 3)
    private String qqNumber;
    @BeanFieldSort(order = 2)
    private String wxNumber;
    @BeanFieldSort(order = 1)
    private String weiboNumber;
}
