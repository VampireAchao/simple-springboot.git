package com.ruben.pojo;

import com.ruben.enumeration.GenderEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: RubenProperties
 * @Description: ruben配置类
 * @Date: 2021/2/16 0016 11:40
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Component
@ConfigurationProperties(prefix = "ruben")
public class RubenProperties {
    private Integer number;
    private String avatar;
    private GenderEnum gender;
    private List<String> hobby;
    private Map<String, Object> introduce;
}
