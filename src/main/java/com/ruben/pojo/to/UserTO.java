package com.ruben.pojo.to;

import com.ruben.pojo.bo.UserBO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: UserTO
 * @Description: 我还没有写描述
 * @Date: 2021/1/23 0023 22:12
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
public class UserTO implements Serializable {
    private static final long serialVersionUID = -2637904684735473209L;
    /**
     * 检索字段：用户名
     */
    private String username;
    /**
     * 页数
     */
    private Integer pageNum;
    /**
     * 当前页数据条数
     */
    private Integer pageSize;
    /**
     * 数据总条数
     */
    private Long total;
    /**
     * 返回BO数据
     */
    private List<UserBO> dataList;

}
