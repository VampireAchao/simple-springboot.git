package com.ruben.pojo.to;

import lombok.*;

import java.io.Serializable;

/**
 * @ClassName: SmsTO
 * @Description: 我还没有写描述
 * @Date: 2021/2/20 0020 21:24
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SmsTO implements Serializable {
    private static final long serialVersionUID = 6355077285571033368L;
    private String number;
    private String code;
}
