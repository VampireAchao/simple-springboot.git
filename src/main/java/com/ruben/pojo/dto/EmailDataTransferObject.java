package com.ruben.pojo.dto;/**
 * @ClassName: EmailDataTransferObject
 * @Date: 2020/11/7 0007 21:50
 * @Description:
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @ClassName: EmailDataTransferObject
 * @Description: 我还没有写描述
 * @Date: 2020/11/7 0007 21:50
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmailDataTransferObject implements Serializable {
    private static final long serialVersionUID = -4297794370277144906L;
    @Email(message = "请输入合法邮箱")
    private String sender;
    @NotBlank(message = "请输入授权码")
    private String code;
    @Email(message = "请输入合法邮箱")
    private String receiver;
    @NotBlank(message = "请输入标题")
    private String title;
    @NotBlank(message = "请输入内容")
    private String content;
    @NotNull(message = "请输入间隔时间(秒)")
    @Min(value = 0, message = "间隔时间不能为负数")
    private Long split;
    @NotNull(message = "请输入发送次数")
    @Min(value = 0, message = "发送次数不能为负数")
    private Long times;
}
