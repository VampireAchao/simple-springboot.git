package com.ruben.pojo.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @ClassName: PageDTO
 * @Description: 我还没有写描述
 * @Date: 2021/1/22 0022 22:51
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
public class PageDTO implements Serializable {
    private static final long serialVersionUID = 1627018090709601488L;
    /**
     * 页数
     */
    private Integer pageNum;
    /**
     * 当前页数据条数
     */
    private Integer pageSize;
    /**
     * 关键字
     */
//    @JsonProperty("keyword")
    @JSONField(name = "keyword")
    private String keywords;
    /**
     * 编号
     */
    private String id;
}
