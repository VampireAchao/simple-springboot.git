package com.ruben.service;


import com.ruben.utils.AjaxJson;

/**
 * @ClassName: ApiOssService
 * @Description:
 * @Date: 2020/5/20 15:14
 * *
 * @author: achao
 * @version: 1.0
 * @since: JDK 1.8
 */
public interface ApiOssService {
    AjaxJson getMark();
}
