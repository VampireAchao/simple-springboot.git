package com.ruben.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruben.pojo.po.UserPO;

public interface MpUserService extends IService<UserPO> {
}
