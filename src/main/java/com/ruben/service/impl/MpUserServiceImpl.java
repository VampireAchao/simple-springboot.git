package com.ruben.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruben.dao.MpUserMapper;
import com.ruben.pojo.po.UserPO;
import com.ruben.service.MpUserService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: MpUserServiceImpl
 * @Description: 我还没有写描述
 * @Date: 2021/3/6 0006 16:42
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Service
public class MpUserServiceImpl extends ServiceImpl<MpUserMapper, UserPO> implements MpUserService {
}
