package com.ruben.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ruben.pojo.User;
import com.ruben.pojo.dto.PageDTO;
import com.ruben.pojo.po.UserPO;
import com.ruben.pojo.to.UserTO;
import com.ruben.utils.AjaxJson;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: UserService
 * @Description:
 * @Date: 2020/8/22 21:17
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public interface UserService {
    /**
     * 登录验证
     *
     * @param user
     * @return
     */
    UserPO validateUser(User user);

    /**
     * 刷新token
     *
     * @param refreshToken
     * @return
     */
    AjaxJson refreshToken(String refreshToken);

    UserPO getUserByUsername(String username);

    String createToken(UserPO user, int expire);

    String getToken(HttpServletRequest request);

    boolean logout(String token);

    String getUsernameByToken(String token);

    UserPO verifyToken(String refreshToken);

    User getUserByUuid(String uuid);

    User createUser(User user);

    UserPO findUserByUsername(String username);

    String register(User user);

    IPage<UserPO> findList(PageDTO pageDTO);

    UserTO findUserList(UserTO param);

    AjaxJson order();
}
