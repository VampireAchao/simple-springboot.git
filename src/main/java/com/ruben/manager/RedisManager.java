package com.ruben.manager;

import com.alibaba.fastjson.TypeReference;

import java.util.function.Supplier;

/**
 * redis管理层
 *
 * @author <achao1441470436@gmail.com>
 * @since 2021/6/11 0011 21:55
 */
public interface RedisManager {

    /**
     * 从缓存中获取否则从mysql中查询
     *
     * @param key           缓存中的key
     * @param mysqlSupplier 查询mysql操作
     * @param typeReference 返回的类型
     * @param <T>           数据类型
     * @return 数据
     */
    <T> T getFromRedisOrPutIntoMysql(String key, Supplier<T> mysqlSupplier, TypeReference<T> typeReference);
}
