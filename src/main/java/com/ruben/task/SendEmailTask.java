package com.ruben.task;/**
 * @ClassName: SendEmailTask
 * @Date: 2020/11/7 0007 21:40
 * @Description:
 */

import com.ruben.pojo.dto.EmailDataTransferObject;
import com.ruben.utils.SendEmail;

import javax.mail.MessagingException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @ClassName: SendEmailTask
 * @Description: 我还没有写描述
 * @Date: 2020/11/7 0007 21:40
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class SendEmailTask {

    public static void sendEmail(EmailDataTransferObject emailDataTransferObject) {
        for (int i = 1; i <= emailDataTransferObject.getTimes(); i++) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        SendEmail.sendEmail(emailDataTransferObject);
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                    System.out.println(emailDataTransferObject);
                }
            }, i * emailDataTransferObject.getSplit() * 1000);
        }
    }

}
