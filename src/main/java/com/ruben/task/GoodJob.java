package com.ruben.task;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

// 避免并发执行注解
@Slf4j
@DisallowConcurrentExecution
public class GoodJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // 任务逻辑
        // 从jobDetail中获取数据
        String name = (String) context.getJobDetail().getJobDataMap().get("name");
        long start = (long) context.getJobDetail().getJobDataMap().get("time");
        // 打印
        log.info(name + " " + (System.nanoTime() - start) / (1000.0 * 1000.0) + "ms");
    }
}
