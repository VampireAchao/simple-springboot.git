package com.ruben.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @ClassName: LogTask
 * @Description: 我还没有写描述
 * @Date: 2020/12/21 0021 20:11
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Slf4j
@Component
public class LogTask {


    //    @Scheduled(cron = "*/5 * * * * ?")
    public void logRecord() {
        log.info("执行了");
    }
}
