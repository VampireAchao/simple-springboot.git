package com.ruben.constant;

/**
 * @ClassName: UserConstant
 * @Description:
 * @Date: 2020/8/22 21:28
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class UserConstant {

    public static final String USER_CACHE = "user_cache";

    public static final String LOGIN_USER_PRE = "login_user_pre_";

    public static final String SALT = "SALT";

    public static final int TOKEN_EXPIRE_TIME = 30000;

    public static final int REFRESH_TOKEN_EXPIRE_TIME = TOKEN_EXPIRE_TIME * 3;

    public static final int TOKEN_REFRESH_TIME = TOKEN_EXPIRE_TIME * 2;

    public static final String TOKEN = "token_";
}
