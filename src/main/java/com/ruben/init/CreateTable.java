package com.ruben.init;

import com.ruben.sql.UserSqlEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @ClassName: CreateTable
 * @Date: 2020/10/11 0011 13:07
 * @Description:
 */
@Slf4j
@Component
public class CreateTable implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        /*if (createUserTable()) {
            log.info("SQLite:创建用户表成功");
        } else {
            log.error("SQLite:创建用户表失败");
        }*/
    }

    public boolean createUserTable() {
        Connection connection = null;
        Statement connectionStatement = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:data.db");
            connectionStatement = connection.createStatement();
            connectionStatement.executeUpdate(UserSqlEnum.CREATE_SQL_LITE.getCommand());
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connectionStatement != null) {
                    connectionStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
