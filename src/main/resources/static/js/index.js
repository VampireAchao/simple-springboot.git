var Ajax = {
    dataFormat: function (data) {
        if (data == null || "" === data) {
            return "";
        }
        return "?" + Object.keys(data).map(function (key) {
            return encodeURIComponent(key) + "=" + encodeURIComponent(data[key]);
        }).join("&");
    },
    // data应为'a=a1&b=b1'这种字符串格式，在jq里如果data为对象会自动将对象转成这种字符串格式
    get: function (url, data, fn) {
        // XMLHttpRequest对象用于在后台与服务器交换数据
        var xhr = new XMLHttpRequest();
        xhr.open('GET',
            url + this.dataFormat(data),
            false);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('token', localStorage.getItem("token"));
        xhr.setRequestHeader('refreshToken', localStorage.getItem("refreshToken"));
        xhr.onreadystatechange = function () {
            // readyState == 4说明请求已完成
            if (xhr.readyState == 4) {
                if (xhr.status == 200 || xhr.status == 304) {
                    var responseText = xhr.responseText
                    var response = JSON.parse(responseText);
                    if (response.code === 402) {
                        var res = {
                            success: function (data) {
                                localStorage.setItem("token", data.token);
                                localStorage.setItem("refreshToken", data.refreshToken);
                                Ajax.get(url, data, fn);
                            },
                            error: function (data) {
                                alert(data.msg);
                                window.location.href = "/index.html"
                            }
                        }
                        Ajax.get("/user/refreshToken?refreshToken=" + localStorage.getItem("refreshToken"), null, res);
                    }
                    if (response.success) {
                        fn.success(response);
                    } else {
                        fn.error(response);
                    }
                }
            }
        }
        xhr.send();
    },

    post: function (url, data, fn) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, false);
        // 添加http头，发送信息至服务器时内容编码类型
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('token', localStorage.getItem("token"));
        xhr.setRequestHeader('refreshToken', localStorage.getItem("refreshToken"));
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200 || xhr.status == 304) {
                    var responseText = xhr.responseText
                    var response = JSON.parse(responseText);
                    if (response.code === 402) {
                        var res = {
                            success: function (data) {
                                localStorage.setItem("token", data.token);
                                localStorage.setItem("refreshToken", data.refreshToken);
                                Ajax.post(url, data, fn);
                            },
                            error: function (data) {
                                alert(data.msg);
                                window.location.href = "/index.html"
                            }
                        }
                        Ajax.get("/user/refreshToken?refreshToken=" + localStorage.getItem("refreshToken"), null, res);
                    }
                    if (response.success) {
                        fn.success(response);
                    } else {
                        fn.error(response);
                    }
                }
            }
        }
        xhr.send(data);
    }
}
